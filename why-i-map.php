<!DOCTYPE html>
<html lang="zxx">



<head>
    <!--====== Required meta tags ======-->
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!--====== Title ======-->
    <title> IMAP - Indian Medicinal and Aromatic Plants (I-MAP) - Industry Association || About </title>

    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="img/png" />
    <!--====== Animate Css ======-->
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <!--====== Bootstrap css ======-->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <!--====== Fontawesome css ======-->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
    <!--====== Flaticon css ======-->
    <link rel="stylesheet" href="assets/css/flaticon.css" />
    <!--====== Slick Css ======-->
    <link rel="stylesheet" href="assets/css/slick.min.css" />
    <!--====== Lity Css ======-->
    <link rel="stylesheet" href="assets/css/lity.min.css" />
    <!--====== Main css ======-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <!--====== Responsive css ======-->
    <link rel="stylesheet" href="assets/css/responsive.css" />
</head>

<style>
		.page-title-area {
	position: relative;
	z-index: 1;
	margin: 145px 0 8px;
    padding: 75px 0;
	background-size: cover;
	background-position: center;
	background-image: url(assets/img/ban2.jpg) !important;
}
	</style>

<body>
    <!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

    <!--====== Preloader ======-->
    <div id="preloader">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object" id="object_one"></div>
                <div class="object" id="object_two"></div>
                <div class="object" id="object_three"></div>
                <div class="object" id="object_four"></div>
            </div>
        </div>
    </div>

    <!--====== Header Start ======-->
    <?php   include("header.php")?>
    <!--====== Header End ======-->

    <!--====== Page Title Start ======-->
    <section class="page-title-area">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-8">
                    <!-- <h1 class="page-title font-40">Why I-MAP</h1> -->
                </div>
                <div class="col-auto">
                    <ul class="page-breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li>Why I-MAP</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--====== Page Title End ======-->

    <!--====== About Section Start ======-->
    <section class="about-section-three section-gap ">
        <div class="container">
            <div class="row align-items-center justify-content-center mb-80">
                <div class="col-xl-12 col-lg-12 col-md-9 col-sm-10">
                    <div class="about-text mb-lg-50">
                        <div class="common-heading mb-30">
                            <span class="tagline">
                                <i class="fas fa-plus"></i> About us
                                <!-- <span class="heading-shadow-text">About Us</span> -->
                            </span>
                            <h2 class="title">Objectives of I-MAP</h2>
                        </div>

                        <ul class="check-list mt-30">
                            <li class="wow fadeInUp" data-wow-delay="0s">
                                <p>Provide support for initiatives around sustainable production and consumption of
                                    medicinal and aromatic plants .</p>
                            </li>
                            <li class="wow fadeInUp" data-wow-delay="0.1s">
                                <p>Provide a platform for exchange of knowledge and ideas related to medicinal and
                                    aromatic plants sector as well as communication and dissemination of value-added
                                    information, sector updates and newsletters etc.</p>
                            </li>
                            <li class="wow fadeInUp" data-wow-delay="0.2s">
                                <p>Foster cooperation, collective and coordinated efforts by sector stakeholders for the
                                    advancement and long-term sustainability of the sector. </p>
                            </li>
                            <li class="wow fadeInUp" data-wow-delay="0.3s">
                                <p>Advocacy of sector’s interests and enabling policies that can work at national,
                                    regional and local level and facilitate sustainable growth of sector.</p>
                            </li>
                            <li class="wow fadeInUp" data-wow-delay="0.4s">
                                <p>Mobilize resources and investments for enhancing the sector’s sustainability
                                    performance.</p>
                            </li>
                            <li class="wow fadeInUp" data-wow-delay="0.5s">
                                <p>Introduce robust system and digital traceability solutions for transparency,
                                    credibility and enhanced efficiency of the value chain. </p>
                            </li>
                            <li class="wow fadeInUp" data-wow-delay="0.6s">
                                <p>Promote sector studies and research on all aspects of medicinal plants and herbal
                                    medicines. </p>
                            </li>
                            <li class="wow fadeInUp" data-wow-delay="0.7s">
                                <p>Promote National Sustainability Standards to raise the floor of sustainable
                                    production which is inclusive, in balance with nature and bringing prosperity to the
                                    producers. </p>
                            </li>
                            <li class="wow fadeInUp" data-wow-delay="0.8s">
                                <p>Support collectors/growers with trainings and capacity building for improved
                                    productivity, efficiency and enable better environmental performance. </p>
                            </li>
                            <li class="wow fadeInUp" data-wow-delay="0.9s">
                                <p>Promote sustainable and scalable market solutions and facilitate market uptake of
                                    sustainably produced products and materials. </p>
                            </li>
                        </ul>


                    </div>
                </div>
                <!-- <div class="col-xl-4 col-lg-8 col-md-10">
                    <div class="about-gallery wow fadeInRight">
                        <div class="img-one">
                            <img src="assets/img/about/about-gallery-1.jpg" alt="Image">
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </section>
    <!--====== About Section End ======-->



    <!--====== Footer Start ======-->
    <?php include("footer.php")?>
    <!--====== Footer End ======-->


    <!--====== jquery js ======-->
    <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <!--====== Bootstrap js ======-->
    <script src="assets/js/bootstrap.min.js"></script>
    <!--====== Inview js ======-->
    <script src="assets/js/jquery.inview.min.js"></script>
    <!--====== Slick js ======-->
    <script src="assets/js/slick.min.js"></script>
    <!--====== Lity js ======-->
    <script src="assets/js/lity.min.js"></script>
    <!--====== Wow js ======-->
    <script src="assets/js/wow.min.js"></script>
    <!--====== Main js ======-->
    <script src="assets/js/main.js"></script>

</body>


</html>