<!DOCTYPE html>
<html lang="zxx">




<head>
    <!--====== Required meta tags ======-->
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!--====== Title ======-->
    <title> IMAP - Indian Medicinal and Aromatic Plants (I-MAP) - Industry Association </title>

    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="img/png" />
    <!--====== Animate Css ======-->
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <!--====== Bootstrap css ======-->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <!--====== Fontawesome css ======-->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
    <!--====== Flaticon css ======-->
    <link rel="stylesheet" href="assets/css/flaticon.css" />
    <!--====== Slick Css ======-->
    <link rel="stylesheet" href="assets/css/slick.min.css" />
    <!--====== Lity Css ======-->
    <link rel="stylesheet" href="assets/css/lity.min.css" />
    <!--====== Main css ======-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <!--====== Responsive css ======-->
    <link rel="stylesheet" href="assets/css/responsive.css" />
</head>

<style>
.page-title-area {
    position: relative;
    z-index: 1;
    margin: 145px 0 8px;
    padding: 75px 0;
    background-size: cover;
    background-position: center;
    background-image: url(assets/img/ban2.jpg) !important;
}

.project-items .project-item .cats {
    margin-top: 35px;
    margin-bottom: 0px;
}

.blog-post-details .post-content {
    background-color: var(--white);
    padding: 35px 26px 80px;
    TEXT-ALIGN: justify;
}

.blog-post-details .post-content .title {
    font-size: 23px;
    margin-bottom: 16px;
    line-height: 1.4;
    text-align: initial;
}

p {
    margin-bottom: 16px;
}

.blog-sidebar .widget.latest-blog-widget li h6 {
    font-size: 12px;
    font-weight: 500;
}
</style>

<body>
    <!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

    <!--====== Preloader ======-->
    <div id="preloader">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object" id="object_one"></div>
                <div class="object" id="object_two"></div>
                <div class="object" id="object_three"></div>
                <div class="object" id="object_four"></div>
            </div>
        </div>
    </div>

    <!--====== Header Start ======-->
    <?php   include("header.php")?>
    <!--====== Header End ======-->

    <!--====== Page Title Start ======-->
    <section class="page-title-area">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-8">
                    <!-- <h1 class="page-title font-40">What We Do</h1> -->
                </div>
                <div class="col-auto">
                    <ul class="page-breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li>Past News</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--====== Page Title End ======-->

    <!--====== About Section Start ======-->
    <section class="blog-area section-gap-extra-bottom primary-soft-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="blog-post-details">
                        <div class="post-thumbnail">
                            <img src="assets/images/activity/4a.jpg" alt="Thumbnail">
                        </div>
                        <div class="post-content">
                            <ul class="post-meta">

                                <li>
                                    <a href="#"><i class="far fa-calendar-alt"></i>25 February 2021</a>
                                </li>
                            </ul>
                            <h3 class="title">
                                The Future Of High-value Medicinal & Aromatic Plants
                            </h3>

                            <p>
                                The importance of medicinal plants and derivatives is growing rapidly with human
                                progress in pharmaceutical fields. These plants are a potential source of bio-molecules
                                that play a major role in modern medicine in the treatment of diseases like cancer,
                                diabetes and hypertension among others. The deactivity for medicinal herbs is increasing
                                thanks in part to a reputation for fewer side effects. They are also considered to be a
                                cost-effective means of developing new and breakthrough drugs.
                            </p>



                            <p>India is a treasure trove of medicinal plants, owing to its rich biodiversity, and a gold
                                mine of medicinal knowledge. It is the second largest exporter of medicinal plants, next
                                to China, in the world and a host to more than three hundred thousand herbal medicine
                                preparations used in ancient healing systems such as Ayurveda, Unani and Homeopathy.
                            </p>
                            <p>
                                In India, a major volume of medicinal herbs come from wild sources. Unsustainable
                                harvesting practices of medicinal plants from the wild often result in rapid degradation
                                of the natural biodiversity and poor regeneration capacity. This in turn affects the
                                production and supply of medicinal plants from forest areas and the quality of the raw
                                materials.
                            </p>
                            <p>
                                An all-encompassing solution lies in cultivating these plants outside forest areas and
                                as a part of existing farm lands. This would also enable farmers and farming communities
                                to enhance their income and livelihood through crop diversification with the high-value
                                medicinal plant species.
                            </p>
                            <p>
                                With that in view as well as integrating circular and regenerative agriculture
                                principles, Solidaridad has started a programme to reclaim sustainability in the
                                medicinal plants and herbal medicines sector through the three facets of the
                                sustainability index: <br>(i) power to the people ('Inclusivity'). <br>(ii) respect for the
                                planet ('Producing in balance with nature'). <br>(iii) a fair share for everyone in the
                                chain ('Prosperity'). </p>
                            <p>
                                The programme 'Sustainable Medicinal & Aromatic Plants' is working towards developing a
                                sustainable value chain of medicinal and aromatic plants (MAPs) in selected districts of
                                Madhya Pradesh and Rajasthan in India through extension of new technologies and
                                practical knowledge among the growers. It is based on a strategy towards promoting a
                                diversified agro-ecosystem within the predominant soy-based cropping system of central
                                India
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="blog-sidebar">

                        <div class="widget latest-blog-widget">
                            <h4 class="widget-title">Activity</h4>
                            <ul>
                            <li>
                                    <div class="thumb">
                                        <img src="assets/images/activity/4a.jpg" alt="Image">
                                    </div>
                                    <div class="desc">
                                        <h6><a href="upcomingevents-details.php">The Future Of High-value Medicinal & Aromatic
                                                Plants</a>
                                        </h6>
                                        <span class="date"><i class="far fa-calendar-alt"></i>25 May 2021</span>
                                    </div>
                                </li>

                                <li>
                                    <div class="thumb">
                                        <img src="assets/images/activity/1.jpg" alt="Image">
                                    </div>
                                    <div class="desc">
                                        <h6><a href="upcomingevents-details1.php">From Low-value Crops To High-value
                                                Market-oriented Maps</a>
                                        </h6>
                                        <span class="date"><i class="far fa-calendar-alt"></i>25 May 2021</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="thumb">
                                        <img src="assets/images/activity/5.jpg" alt="Image">
                                    </div>
                                    <div class="desc">
                                        <h6><a href="upcomingevents-details2.php">Rising Income And Livelihood Conditions</a>
                                        </h6>
                                        <span class="date"><i class="far fa-calendar-alt"></i>22 Oct 2021</span>
                                    </div>
                                </li>

                                <li>
                                    <div class="thumb">
                                        <img src="assets/images/activity/2.jpg" alt="Image">
                                    </div>
                                    <div class="desc">
                                        <h6><a href="upcomingevents-details3.php">Enabling Market Access And
                                                Entrepreneurship</a>
                                        </h6>
                                        <span class="date"><i class="far fa-calendar-alt"></i>22 Oct 2021</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="thumb">
                                        <img src="assets/images/activity/3.jpg" alt="Image">
                                    </div>
                                    <div class="desc">
                                        <h6><a href="upcomingevents-details4.php">Towards Prosperity, Inclusivity And Balanced
                                                Production</a>
                                        </h6>
                                        <span class="date"><i class="far fa-calendar-alt"></i>22 Oct 2021</span>
                                    </div>
                                </li>


                            </ul>
                        </div>

                        <div class="widget cta-widget">
                            <div class="cta-content">
                                <h4 class="title">Become <br> Member</h4>

                                <p>
                                    you can join us and become member by filling this form
                                </p>
                                <a href="IMAP_Membership_Form.pdf" class="cta-button">Join Now</a>


                                <div class="cta-img">
                                    <img src="assets/images/become.jpg" alt="Image">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== About Section End ======-->



    <!--====== Footer Start ======-->
    <?php include("footer.php")?>
    <!--====== Footer End ======-->


    <!--====== jquery js ======-->
    <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <!--====== Bootstrap js ======-->
    <script src="assets/js/bootstrap.min.js"></script>
    <!--====== Inview js ======-->
    <script src="assets/js/jquery.inview.min.js"></script>
    <!--====== Slick js ======-->
    <script src="assets/js/slick.min.js"></script>
    <!--====== Lity js ======-->
    <script src="assets/js/lity.min.js"></script>
    <!--====== Wow js ======-->
    <script src="assets/js/wow.min.js"></script>
    <!--====== Main js ======-->
    <script src="assets/js/main.js"></script>

</body>




</html>