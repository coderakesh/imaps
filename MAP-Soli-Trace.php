<!DOCTYPE html>
<html lang="zxx">


<!-- Mirrored from demo.webtend.net/html/funden/project-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 17 Nov 2021 07:16:15 GMT -->

<head>
    <!--====== Required meta tags ======-->
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!--====== Title ======-->
    <title> IMAP - Indian Medicinal and Aromatic Plants (I-MAP) - Industry Association  || Digital Solution </title>

    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="img/png" />
    <!--====== Animate Css ======-->
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <!--====== Bootstrap css ======-->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <!--====== Fontawesome css ======-->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
    <!--====== Flaticon css ======-->
    <link rel="stylesheet" href="assets/css/flaticon.css" />
    <!--====== Slick Css ======-->
    <link rel="stylesheet" href="assets/css/slick.min.css" />
    <!--====== Lity Css ======-->
    <link rel="stylesheet" href="assets/css/lity.min.css" />
    <!--====== Main css ======-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <!--====== Responsive css ======-->
    <link rel="stylesheet" href="assets/css/responsive.css" />
</head>

<style>
.page-title span {
    background: linear-gradient(#fc0, #fc0) no-repeat;
    background-position: 0px 90%;
    background-size: 100% 20%;
}
.project-items.project-style-two .project-item .title {
    font-size: 18px;
    margin-bottom: 10px;
}
.project-items .project-item .stats-value{
	font-size:14px !important;
}
.thumb{
	padding: 0px 15px;
}
.thumb img{
	clip-path: polygon(0 1%, 22% 1%, 48% 2%, 72% 0%, 100% 1%, 100% 98%, 81% 97%, 44% 99%, 11% 99%, 0% 98%);
	height: 277px;
}
.img-text h4{
	text-align: center;
    background: #ffcc00;
}
.cats{
	float:right !important;
}
.project-items .project-item .cats a {
    -webkit-box-shadow: none;
    box-shadow: none;
}
.page-title-area {
	position: relative;
	z-index: 1;
	margin: 145px 0 25px;padding: 55px 0;
	background-size: cover;
	background-position: center;
	background-image: url(assets/img/ban1.jpg) !important;
}


</style>

<body>
    <!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

    <!--====== Preloader ======-->
    <div id="preloader">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object" id="object_one"></div>
                <div class="object" id="object_two"></div>
                <div class="object" id="object_three"></div>
                <div class="object" id="object_four"></div>
            </div>
        </div>
    </div>

    <!--====== Header Start ======-->
    <?php   include("header.php")?>
    <!--====== Header End ======-->

    <!--====== Page Title Start ======-->
    <section class="page-title-area ">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-8">
                    <!-- <h1 class="page-title"><span> MAP </span> <span> Soli </span> <span> Trace </span></h1> -->
                </div>
                <div class="col-auto">
                    <ul class="page-breadcrumb">
                        <li><a href="index-2.html">Home</a></li>
                        <li> MAP Soli Trace</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--====== Page Title End ======-->

    <!--====== Project Area Start ======-->
    <section class="project-section section-gap-extra-bottom primary-soft-bg">
        <div class="container">
            <div class="row justify-content-center project-items project-style-two">
                <div class="col-xl-10">
                    <div class="project-item mb-30 row">
                        <div class="thumb col-xs-6" >
						<img src="assets/img/digi_solution/1.jpg" alt="" style="">
							<div class="img-text" >
								<h4>BHARADWAJ HERBS AND AYURVEDA</h4>
							</div>
                        </div>
                        <div class="col-xl-6">
                            <h5 class="title">
                                <a href="project-details.html">SOLIDARIDAD STRATEGIES TOWARRD RECLAIMING SUSTAINABILITY
                                    IN MEDICINAL PLANTS AND HERBAL MEDICINE SECTOR </a>
                            </h5>
							<div class="project-stats">
                                <div class="stats-value">
                                    <span class="value-title">Solidaridad being a global civil society organization has
                                        been frontrunner in promotion and implementation of innovate market-based
                                        sustainability solutions since last five decades. The organization is pioneer in
                                        facilitating the development of socially responsible, ecologically sound and
                                        profitable supply chains across various global commodities. </span>
                                </div>
                            </div>

                            <div class="cats mt-30">
							<a href="#" class="theme-btn">Read More<i class="fa fa-angle-double-right"
                                aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="col-xl-10">
                    <div class="project-item mb-30 row">
                        <div class="thumb col-xs-6" >
						<img src="assets/img/digi_solution/1.jpg" alt="" style="">
							<div class="img-text" >
								<h4>BHARADWAJ HERBS AND AYURVEDA</h4>
							</div>
                        </div>
                        <div class="col-xl-6">
                            <h5 class="title">
                                <a href="project-details.html">SOLIDARIDAD STRATEGIES TOWARRD RECLAIMING SUSTAINABILITY
                                    IN MEDICINAL PLANTS AND HERBAL MEDICINE SECTOR </a>
                            </h5>
							<div class="project-stats">
                                <div class="stats-value">
                                    <span class="value-title">Solidaridad being a global civil society organization has
                                        been frontrunner in promotion and implementation of innovate market-based
                                        sustainability solutions since last five decades. The organization is pioneer in
                                        facilitating the development of socially responsible, ecologically sound and
                                        profitable supply chains across various global commodities. </span>
                                </div>
                            </div>

                            <div class="cats mt-30">
							<a href="#" class="theme-btn">Read More<i class="fa fa-angle-double-right"
                                aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="view-more-btn text-center mt-40">
                        <a href="#" class="main-btn bordered-btn">View More Project <i
                                class="far fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== Project Area End ======-->

    <!--====== Footer Start ======-->
    <?php   include("footer.php")?>
    <!--====== Footer End ======-->

    <!--====== jquery js ======-->
    <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <!--====== Bootstrap js ======-->
    <script src="assets/js/bootstrap.min.js"></script>
    <!--====== Inview js ======-->
    <script src="assets/js/jquery.inview.min.js"></script>
    <!--====== Slick js ======-->
    <script src="assets/js/slick.min.js"></script>
    <!--====== Lity js ======-->
    <script src="assets/js/lity.min.js"></script>
    <!--====== Wow js ======-->
    <script src="assets/js/wow.min.js"></script>
    <!--====== Main js ======-->
    <script src="assets/js/main.js"></script>

</body>


<!-- Mirrored from demo.webtend.net/html/funden/project-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 17 Nov 2021 07:16:15 GMT -->

</html>