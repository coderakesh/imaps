<!DOCTYPE html>
<html lang="zxx">


<head>
	<!--====== Required meta tags ======-->
	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge" />
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<!--====== Title ======-->
	<title> IMAP - Indian Medicinal and Aromatic Plants (I-MAP) - Industry Association  </title>

	<!--====== Favicon Icon ======-->
	<link rel="shortcut icon" href="assets/img/favicon.ico" type="img/png" />
	<!--====== Animate Css ======-->
	<link rel="stylesheet" href="assets/css/animate.min.css">
	<!--====== Bootstrap css ======-->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
	<!--====== Fontawesome css ======-->
	<link rel="stylesheet" href="assets/css/font-awesome.min.css" />
	<!--====== Flaticon css ======-->
	<link rel="stylesheet" href="assets/css/flaticon.css" />
	<!--====== Slick Css ======-->
	<link rel="stylesheet" href="assets/css/slick.min.css" />
	<!--====== Lity Css ======-->
	<link rel="stylesheet" href="assets/css/lity.min.css" />
	<!--====== Main css ======-->
	<link rel="stylesheet" href="assets/css/main.css" />
	<!--====== Responsive css ======-->
	<link rel="stylesheet" href="assets/css/responsive.css" />

	<style>
		.page-title-area {
	position: relative;
	z-index: 1;
	margin: 145px 0 8px;
    padding: 75px 0;
	background-size: cover;
	background-position: center;
	background-image: url(assets/img/ban2.jpg) !important;
}

.mtop{
margin-top: -33px;
}

@media (max-width: 767.98px) { 	.mtop{
margin-top: 0px;

}
}


.contact-form {
    background-color: var(--primary-soft-color);
    padding: 46px 23px;
}
input, textarea, select {
    width: 100%;
    background-color: var(--white);
    padding: 0 9px;
    height: 35px;
    font-size: 16px;
    color: #afafaf;
    -webkit-transition: 0.3s;
    -o-transition: 0.3s;
    transition: 0.3s;
    border: 2px solid var(--border-color);
}
	</style>

</head>

<body>
	<!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

	<!--====== Preloader ======-->
	<div id="preloader">
		<div id="loading-center">
			<div id="loading-center-absolute">
				<div class="object" id="object_one"></div>
				<div class="object" id="object_two"></div>
				<div class="object" id="object_three"></div>
				<div class="object" id="object_four"></div>
			</div>
		</div>
	</div>

	<!--====== Header Start ======-->
	<?php   include("header.php")?>
	<!--====== Header End ======-->

	<!--====== Page Title Start ======-->
	<section class="page-title-area">
		<div class="container">
			<div class="row align-items-center justify-content-between">
				<div class="col-lg-8">
					<!-- <h1 class="page-title font-40">Who We Are</h1> -->
				</div>
				<div class="col-auto">
					<ul class="page-breadcrumb">
						<li><a href="index.php">Home</a></li>
						<li>Contact</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--====== Page Title End ======-->
	
		<!--====== About Section Start ======-->
		<section class="contact-section section-gap-extra-bottom">
        <div class="container">
            <!-- Contact Info Start -->
            <div class="row align-items-center justify-content-center">
			<div class="col-lg-5">
                        <div class="contact-form">
                            <form action="#">
                                <h3 class="form-title">Send Us Message</h3>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-field mb-25">
                                            <label for="name">Your Name</label>
                                            <input type="text" placeholder="Your Name" id="name">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-field mb-25">
                                            <label for="phone-number">Phone Number</label>
                                            <input type="text" placeholder="Phone Number" id="phone-number">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-field mb-25">
                                            <label for="email">Email Address</label>
                                            <input type="text" placeholder="support@gmail.com" id="email">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-field mb-25">
                                            <label for="subject">Subject</label>
                                            <input type="text" placeholder="I would like to" id="subject">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-field mb-30">
                                            <label for="message">Write Message</label>
                                            <textarea id="message" placeholder="Hello"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-field">
                                            <button class="main-btn">Send Us Message <i class="far fa-arrow-right"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                <div class="col-xl-6 ">
                    <div class="contact-info-boxes">
                        <div class="row justify-content-center align-items-center mtop">
                            <div class="col-md-6 col-sm-10">
                                <div class="info-box text-center wow fadeInUp" data-wow-delay="0.2s">
                                    <div class="icon">
                                        <i class="flaticon-place"></i>
                                    </div>
                                    <div class="info-content">
                                        <h5>Our Location</h5>
                                        <p>
										Solidaridad <br> Shreenath Kripa Apartment,
Ground floor, D-26, B.D.A. Colony,<br>
 Koh-E-Fiza Bhopal 
(M.P) - 462001 (INDIA)
                                        </p>
                                    </div>
                                </div>
                                <div class="info-box text-center mt-30 mb-sm-30 wow fadeInUp" data-wow-delay="0.3s">
                                    <div class="icon">
                                        <i class="flaticon-envelope"></i>
                                    </div>
                                    <div class="info-content">
                                        <h5>Email Address</h5>
                                        <p>
                                            <a href="#" class="__cf_email__" data-cfemail="7d0e080d0d120f0914131b123d1a101c1411531e1210">info@imap.com</a> <br>
                                            
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-10">
                                <div class="info-box text-center wow fadeInUp" data-wow-delay="0.3s">
                                    <div class="icon">
                                        <i class="flaticon-phone-call-1"></i>
                                    </div>
                                    <div class="info-content">
                                        <h5>Contact No.</h5>
                                        <p>
                                        +91-(0)755 2548160<br>
                                            
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Contact Info End -->
      
        </div>
    </section>
		<!--====== About Section End ======-->



	 <!--====== Footer Start ======-->
	 <?php include("footer.php")?>
    <!--====== Footer End ======-->


	<!--====== jquery js ======-->
</script><script src="assets/js/jquery.min.js"></script>
	<!--====== Bootstrap js ======-->
	<script src="assets/js/bootstrap.min.js"></script>
	<!--====== Inview js ======-->
	<script src="assets/js/jquery.inview.min.js"></script>
	<!--====== Slick js ======-->
	<script src="assets/js/slick.min.js"></script>
	<!--====== Lity js ======-->
	<script src="assets/js/lity.min.js"></script>
	<!--====== Wow js ======-->
	<script src="assets/js/wow.min.js"></script>
	<!--====== Main js ======-->
	<script src="assets/js/main.js"></script>

</body>

</html>