<footer class="site-footer with-footer-cta with-footer-bg">
		<div class="footer-cta">
			<div class="container">
				<div class="row justify-content-lg-between justify-content-center align-items-center">
					<div class="col-lg-9 col-md-8 col-sm-10">
						<span class="cta-tagline">Since 25 Years</span>
						<h3 class="cta-title">Significantly contribute towards the global</h3>
					</div>
					<div class="col-lg-auto col-md-6">
						<a href="workinprogress.php." class="main-btn main-btn-css bordered-btn bordered-white mt-md-30">
							Know More <i class="far fa-arrow-right"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-content-area">
			<div class="container">
				<div class="footer-widgets">
					<div class="row justify-content-between">
						<div class="col-xl-5 col-lg-4 col-md-6">
							<div class="widget about-widget">
								<div class="footer-logo">
									<img src="assets/img/footer-logo.png" alt="Funden">
								</div>
								<p>
									Medicinal & Aromatic Plants sector significantly contribute towords the global, National & local economies. 
								</p>
								<div class="newsletter-form">
									<h5 class="form-title">Join Newsletters</h5>
									<form action="#">
										<input type="text" placeholder="Email Address">
										<button type="submit"><i class="far fa-arrow-right"></i></button>
									</form>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-5 col-sm-6">
							<div class="widget nav-widget">
								<h4 class="widget-title">Quick Links</h4>
								<ul>
									<li><a href="who-we-are.php">About Us</a></li>
									<li><a href="what-we-do.php">What we do </a></li>
									<li><a href="upcomingevents&activity.php">Activities</a></li>
									<li><a href="gallery.php">Gallery</a></li>
									
									<li><a href="publication.php">Publications </a></li>
									<li><a href="contact.php">Contact us</a></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4">
							<div class="widget contact-widget">
								<h4 class="widget-title">Contact Us</h4>
								<ul class="info-list">
									<li>
										<span class="icon"><i class="far fa-phone"></i></span>
										<span class="info">
											<span class="info-title">Phone Number</span>
											<a href="tel:07552548160">+91-(0)755 2548160</a>
										</span>
									</li>
									<li>
										<span class="icon"><i class="far fa-envelope-open"></i></span>
										<span class="info">
											<span class="info-title">Email Address</span>
											<a href="#"><span class="__cf_email__" data-cfemail="ef9c9a9f9f809d9baf88828e8683c18c8082">info@imap.com</span></a>
										</span>
									</li>
									<li>
										<span class="icon"><i class="far fa-map-marker-alt"></i></span>
										<span class="info">
											<span class="info-title">Locations</span>
											<a href="#">Solidaridad <br> Shreenath Kripa Apartment,
Ground floor, D-26, B.D.A. Colony,
 Koh-E-Fiza Bhopal 
(M.P) - 462001 (INDIA)   </a>
										</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="copyright-area">
					<div class="row flex-md-row-reverse">
						<div class="col-md-6">
							<ul class="social-icons">
								<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="#"><i class="fab fa-twitter"></i></a></li>
								<li><a href="#"><i class="fab fa-youtube"></i></a></li> 
								<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
							</ul>
						</div>
						<div class="col-md-6">
							<p class="copyright-text">© 2021 <a href="#">I-MAP</a>. All Rights Reserved</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>