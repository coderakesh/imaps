<link rel="shortcut icon" href="assets/img/favicon.ico" type="img/png" />
	<!--====== Animate Css ======-->
	<link rel="stylesheet" href="assets/css/animate.min.css">
	<!--====== Bootstrap css ======-->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
	<!--====== Fontawesome css ======-->
	<link rel="stylesheet" href="assets/css/font-awesome.min.css" />
	<!--====== Flaticon css ======-->
	<link rel="stylesheet" href="assets/css/flaticon.css" />
	<!--====== Slick Css ======-->
	<link rel="stylesheet" href="assets/css/slick.min.css" />
	<!--====== Lity Css ======-->
	<link rel="stylesheet" href="assets/css/lity.min.css" />
	<!--====== Main css ======-->
	<link rel="stylesheet" href="assets/css/main.css" />
	<!--====== Responsive css ======-->
	<link rel="stylesheet" href="assets/css/responsive.css" /><link href="dist/assets/owl.carousel.css" rel="stylesheet"> 
   <style>

        .hide{display:none !important;}

		.theme-btn, .theme-btn-s2 {
  color: #fff;
  font-size: 18px;
  font-weight: 500;
  display: inline-block;
  padding: 12px 60px 12px 30px;
  border: 0;
  -webkit-border-radius: 0;
  -moz-border-radius: 0;
  -o-border-radius: 0;
  -ms-border-radius: 0;
  border-radius: 40px;
  position: relative;
  background: -webkit-linear-gradient(left,#1ab75f,#e6bf3e 50%,#18b760);
  background-size: 200%,1px;
  background-position: 0;
  transition: all .3s;
  -moz-transition: all .3s;
  -webkit-transition: all .3s;
}

.theme-btn:hover, .theme-btn-s2:hover {
  background-position: 100%!important;
  color: #fff;
}

.theme-btn i, .theme-btn-s2 i {
  position: absolute;
  right: 30px;
  top: 50%;
  -webkit-transform: translateY(-50%);
  -moz-transform: translateY(-50%);
  -o-transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}
.theme-btn-s2{
  background: -webkit-linear-gradient(left,#14b761,#3ac060 50%,#14b761);
  color: #fff;
  padding: 16px 30px 16px 30px;
}
.theme-btn:hover, .theme-btn-s2:hover, .theme-btn:focus, .theme-btn-s2:focus, .theme-btn:active, .theme-btn-s2:active {
  background-position: 100%!important;
  color: #fff;
}

@media (max-width: 767px) {
  .theme-btn, .theme-btn-s2 {
    padding: 12px 16px;
    font-size: 15px;
  }
  .theme-btn i, .theme-btn-s2 i {
    display: none;
  }
}

    </style>