<!DOCTYPE html>
<html lang="zxx">




<head>
    <!--====== Required meta tags ======-->
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!--====== Title ======-->
    <title> IMAP - Indian Medicinal and Aromatic Plants (I-MAP) - Industry Association  </title>

    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="img/png" />
    <!--====== Animate Css ======-->
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <!--====== Bootstrap css ======-->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <!--====== Fontawesome css ======-->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
    <!--====== Flaticon css ======-->
    <link rel="stylesheet" href="assets/css/flaticon.css" />
    <!--====== Slick Css ======-->
    <link rel="stylesheet" href="assets/css/slick.min.css" />
    <!--====== Lity Css ======-->
    <link rel="stylesheet" href="assets/css/lity.min.css" />
    <!--====== Main css ======-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <!--====== Responsive css ======-->
    <link rel="stylesheet" href="assets/css/responsive.css" />
</head>

<style>
.page-title-area {
    position: relative;
    z-index: 1;
    margin: 145px 0 8px;
    padding: 75px 0;
    background-size: cover;
    background-position: center;
    background-image: url(assets/img/ban2.jpg) !important;
}

.project-items .project-item .cats {
    margin-top: 35px;
    margin-bottom: 0px;
}

.faq-accordion-tab .nav {
    padding: 10px 18px;
    background-color: #fdbf00;
    margin: 1px;
    display: -webkit-inline-box;
    display: -ms-inline-flexbox;
    display: inline-flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
}

.faq-accordion-tab .tab-nav-area {
    margin-bottom: 35px;
    background-color: #fdbf00;
}

.event-content p {
    line-height: 26px;
    margin-bottom: 5px;
    font-size: 15px;
    font-weight: 500;

}
.category {
    background-color: var(--third-color);
    color: var(--white);
    font-size: 14px;
    font-weight: 600;
    padding: 7px 10px;
    border-radius: 6px;
    margin-right: 10px;
}
p {
    margin: 0px;
    text-align: justify;
}
.blog-sidebar {
    padding-left: 0px; 
}
.section-gap-extra-bottom {
    padding-top: 15px;
    padding-bottom: 80px;
}
.blog-sidebar .widget.latest-blog-widget li h6 {
    font-size: 12px;
    font-weight: 500;
}
.project-items.project-style-two .project-item {

    border-radius: 15px;
}
</style>

<body>
    <!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

    <!--====== Preloader ======-->
    <div id="preloader">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object" id="object_one"></div>
                <div class="object" id="object_two"></div>
                <div class="object" id="object_three"></div>
                <div class="object" id="object_four"></div>
            </div>
        </div>
    </div>

    <!--====== Header Start ======-->
    <?php   include("header.php")?>
    <!--====== Header End ======-->

    <!--====== Page Title Start ======-->
    <section class="page-title-area">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-8">
                    <!-- <h1 class="page-title font-40">What We Do</h1> -->
                </div>
                <div class="col-auto">
                    <ul class="page-breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li>Upcomingevents&amp;Activity</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--====== Page Title End ======-->

    <!--====== About Section Start ======-->
    <section class="faq-section section-gap">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-12 col-lg-12">
                    <div class="faq-accordion-tab">
                        <div class="tab-nav-area text-center">
                            <ul class="nav nav-tabs" id="faqTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#general"
                                        role="tab">Activities</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#speakers" role="tab">Upcomingevents</a>
                                </li>

                            </ul>
                        </div>
                        <div class="tab-content" id="faqTabContent">
                            <div class="tab-pane fade show active" id="general" role="tabpanel">
                            <section class="project-section section-gap-extra-bottom primary-soft-bg">
		<div class="container">
			<div class="row justify-content-center project-items project-style-two">
				<div class="col-xl-10">
					<div class="project-item mb-30">
						<div class="thumb" style="background-image: url(assets/images/activity/4.jpg);"></div>
						<div class="content">
							
							
							<h5 class="title">
								<a href="upcomingevents-details.php">The Future Of High-value Medicinal & Aromatic Plants</a>
							</h5>
							
							<span class="date"><i class="far fa-calendar-alt"></i> 25 February 2021</span>
							<div class="cats">
								<a href="upcomingevents-details.php">View details</a>
							</div>
						</div>
					</div>
				</div>
                <div class="col-xl-10">
					<div class="project-item mb-30">
						<div class="thumb" style="background-image: url(assets/images/activity/1.jpg);"></div>
						<div class="content">
							
							
							<h5 class="title">
								<a href="upcomingevents-details1.php">From Low-value Crops To High-value Market-oriented Maps</a>
							</h5>
							
							<span class="date"><i class="far fa-calendar-alt"></i> 25 February 2021</span>
							<div class="cats">
								<a href="upcomingevents-details1.php">View details</a>
							</div>
						</div>
					</div>
				</div>
                <div class="col-xl-10">
					<div class="project-item mb-30">
						<div class="thumb" style="background-image: url(assets/images/activity/5.jpg);"></div>
						<div class="content">
							
							
							<h5 class="title">
								<a href="upcomingevents-details2.php">Rising Income And Livelihood Conditions</a>
							</h5>
							
							<span class="date"><i class="far fa-calendar-alt"></i> 25 February 2021</span>
							<div class="cats">
								<a href="upcomingevents-details2.php">View details</a>
							</div>
						</div>
					</div>
				</div>
                <div class="col-xl-10">
					<div class="project-item mb-30">
						<div class="thumb" style="background-image: url(assets/images/activity/2.jpg);"></div>
						<div class="content">
							
							
							<h5 class="title">
								<a href="upcomingevents-details3.php">Enabling Market Access And Entrepreneurship</a>
							</h5>
							
							<span class="date"><i class="far fa-calendar-alt"></i> 25 February 2021</span>
							<div class="cats">
								<a href="upcomingevents-details3.php">View details</a>
							</div>
						</div>
					</div>
				</div>
                <div class="col-xl-10">
					<div class="project-item mb-30">
						<div class="thumb" style="background-image: url(assets/images/activity/3.jpg);"></div>
						<div class="content">
							
							
							<h5 class="title">
								<a href="upcomingevents-details4.php">Towards Prosperity, Inclusivity And Balanced Production</a>
							</h5>
							
							<span class="date"><i class="far fa-calendar-alt"></i> 25 February 2021</span>
							<div class="cats">
								<a href="upcomingevents-details4.php">View details</a>
							</div>
						</div>
					</div>
				</div>
               
                
			</div>
		</div>
	</section>
                            </div>
                            <div class="tab-pane fade" id="speakers" role="tabpanel">
                                <section class="event-area section-gap-extra-bottom">
                                    <div class="container">
                                        <div class="event-items">
                                            <div class="single-event-item mb-30 ">

                                                <div class="event-content " style="width:100%">


                                                    <h4 class="event-title"><span  class="category">Upcomingevents</span>Capacity Building of farmers and
                                                        Collectors </h4>
                                                    <p>
                                                        Medicinal and Aromatic crop growers desiring for certification
                                                        of their farm produce, are being facilitated by the I-MAP to get
                                                        certification from quality council of India under “Voluntary
                                                        Certification Scheme for Medicinal Plant Produce”. For the Rabi
                                                        season of this financial year, 4 numbers of group will be
                                                        facilitated to apply for this scheme from Mandsaur, Ratlam,
                                                        Vidisha and Neemach districts of Madhya Pradesh.
                                                        This scheme has developed in collaboration of Quality Council of
                                                        India and The National Medicinal Plants Board (NMPB). Based on
                                                        the good agricultural and collection practices to enhance
                                                        confidence in the quality of India's medicinal plant produce and
                                                        make available good quality raw material to the AYUSH industry.
                                                    </p>
                                                </div>

                                            </div>
                                            <div class="single-event-item mb-30 ">

                                                <div class="event-content">

                                                    <h4 class="event-title"><span  class="category">Upcomingevents</span> Skill Development training of Farmers</h4>
                                                    <p>
                                                        -MAP and Solidaridad Regional expertise Centre, had planned to
                                                        conduct the skill development training programme for the MAP
                                                        grower farmers. Basic objective of the training programme is to
                                                        build the capacity of the farmers and to develop their skill to
                                                        provide adequate training so that it could help them to generate
                                                        additional income. Apart from this, it is also being planned to
                                                        increase the diversification of crop for sustainable production
                                                        of medicinal and aromatic crop, and to increase the awareness of
                                                        farmers on methods of processing of medicinal and aromatic
                                                        crops. </p>
                                                    <p>
                                                        This skill development training programme will provide the
                                                        information to the young farmers about some important medicinal
                                                        plants for cultivation in terms of business and good prices.
                                                        Addition to that, this training programme will be certified by
                                                        the I-MAP.
                                                    </p>
                                                </div>

                                            </div>


                                            
                    
                                        </div>
                                    </div>
                                </section>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== About Section End ======-->



    <!--====== Footer Start ======-->
    <?php include("footer.php")?>
    <!--====== Footer End ======-->


    <!--====== jquery js ======-->
    <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <!--====== Bootstrap js ======-->
    <script src="assets/js/bootstrap.min.js"></script>
    <!--====== Inview js ======-->
    <script src="assets/js/jquery.inview.min.js"></script>
    <!--====== Slick js ======-->
    <script src="assets/js/slick.min.js"></script>
    <!--====== Lity js ======-->
    <script src="assets/js/lity.min.js"></script>
    <!--====== Wow js ======-->
    <script src="assets/js/wow.min.js"></script>
    <!--====== Main js ======-->
    <script src="assets/js/main.js"></script>

</body>




</html>