<!--====== Header Start ======-->
<style>.sticky-on .site-logo img {
    padding: 6px 0;
    width: 87%;
}</style>
<header class="site-header sticky-header transparent-header">
    <div class="header-topbar topbar-secondary-bg d-none d-sm-block">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-auto">
                    <ul class="contact-info">
                        <li><a href="#"><i class="far fa-envelope"></i> <span class="__cf_email__"
                                    data-cfemail="7a090f0a0a15080e3a1d171b131654191517">support@gmail.com</span></a>
                        </li>
                    </ul>
                </div>
                <div class="col-auto d-none d-md-block">
                    <ul class="social-icons">
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                        <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="navbar-wrapper">
        <div class="container">
            <div class="navbar-inner">
                <div class="site-logo">
                    <a href="index.php" class="d-none d-sm-block"><img src="assets/img/logo.png"
                            alt="Indian Medicinal and Aromatic Plants"></a>
                    <a href="index.php" class="d-block d-sm-none"><img src="assets/img/mobile-logo.png"
                            alt="Indian Medicinal and Aromatic Plants"></a>
                </div>
                <div class="nav-menu">
                    <ul>
                        <li class="current">
                            <a href="index.php">Home</a>

                        </li>
                        <li>
                            <a href="#.">About Us</a>
                            <ul class="submenu">
                                <li><a href="who-we-are.php">Who we are </a></li>
                                <li><a href="what-we-do.php">What we do</a></li>
                                <li><a href="why-i-map.php">Why I-MAP</a></li>

                            </ul>
                        </li>

                        <li>
                            <a href="#.">Members</a>
                            <ul class="submenu">
                                <li><a href="workinprogress.php">Members</a></li>
                                <li><a href="workinprogress.php">Members Initiatives</a></li>
                                <li><a href="IMAP_Membership_Form.pdf">Become a Member</a></li>

                            </ul>
                        </li>
                        <li>
                            <a href="#.">Activities</a>
                            <ul class="submenu">
                                <li><a href="pastnews&activity.php">Past Events & Activities</a></li>
                                <li><a href="upcomingevents&activity.php">Upcoming Events & Activities</a></li>

                            </ul>
                        </li>

                        <li>
                            <a href="#.">Traceability</a>
                            <ul class="submenu">
                                <!--<li><a href="digital-solution.php">Digital Solutions</a></li>-->
                                <li><a href="traceability.php">MAP - Soli- Trace</a></li>
                                <!--<li><a href="E-Learning.php">E-Learning</a></li>-->

                            </ul>
                        </li>
                        <li>
                            <a href="#.">News & Updates</a>
                            <ul class="submenu">
                                <li><a href="workinprogress.php">Latest News</a></li>
                                <li><a href="pastnews.php">Past News</a></li>

                            </ul>
                        </li>
                        <li>
                            <a href="#.">Resources</a>
                            <ul class="submenu">
                                <li><a href="publication.php">Publications </a></li>
                                <li><a href="workinprogress.php">Articles</a></li>

                            </ul>
                        </li>




                        <li><a href="contact.php">Contact</a></li>
                    </ul>
                </div>
                <div class="navbar-extra d-flex align-items-center">

                    <a href="#" class="nav-toggler">
                        <span></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="mobile-menu-panel">
        <div class="panel-logo">
            <a href="index.php"><img src="assets/img/logo-white.png" alt="Indian Medicinal and Aromatic Plants"></a>
        </div>
        <ul class="panel-menu">
            <li class="current">
                <a href="index.php">Home</a>

            </li>
            <li>
                <a href="#.">About Us</a>
                <ul class="submenu">
                    <li><a href="who-we-are.php">Who we are </a></li>
                    <li><a href="what-we-do.php">What we do</a></li>
                    <li><a href="why-i-map.php">Why I-MAP</a></li>

                </ul>
            </li>

            <li>
                <a href="#.">Members</a>
                <ul class="submenu">
                <li><a href="workinprogress.php">Members</a></li>
                                <li><a href="workinprogress.php">Members Initiatives</a></li>
                                <li><a href="IMAP_Membership_Form.pdf">Become a Member</a></li>

                </ul>
            </li>
            <li>
                <a href="#.">Activities</a>
                <ul class="submenu">
                    <li><a href="pastnews&activity.php">Past Events & Activities</a></li>
                    <li><a href="upcomingevents&activity.php">Upcoming Events & Activities</a></li>

                </ul>
            </li>

            <li>
                <a href="#.">Digital Solutions</a>
                <ul class="submenu">
                   <!--<li><a href="digital-solution.php">Digital Solutions</a></li>-->
                   <li><a href="traceability.php">MAP - Soli- Trace</a></li>
                                <!--<li><a href="E-Learning.php">E-Learning</a></li>-->

                </ul>
            </li>
            <li>
                <a href="#.">News & Updates</a>
                <ul class="submenu">
                    <li><a href="latest-news.php">Latest News</a></li>
                    <li><a href="pastnews.php">Past News</a></li>

                </ul>
            </li>
            <li>
                <a href="#.">Resources</a>
                <ul class="submenu">
                    <li><a href="publication.php">Publications </a></li>
                    <li><a href="articles.php">Articles</a></li>

                </ul>
            </li>
            <li><a href="contact.php">Contact</a></li>
        </ul>
        <a href="#" class="panel-close">
            <i class="fal fa-times"></i>
        </a>
    </div>
</header>
<!--====== Header End ======-->