<!DOCTYPE html>
<html lang="zxx">




<head>
    <!--====== Required meta tags ======-->
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!--====== Title ======-->
    <title> IMAP - Indian Medicinal and Aromatic Plants (I-MAP) - Industry Association </title>

    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="img/png" />
    <!--====== Animate Css ======-->
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <!--====== Bootstrap css ======-->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <!--====== Fontawesome css ======-->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
    <!--====== Flaticon css ======-->
    <link rel="stylesheet" href="assets/css/flaticon.css" />
    <!--====== Slick Css ======-->
    <link rel="stylesheet" href="assets/css/slick.min.css" />
    <!--====== Lity Css ======-->
    <link rel="stylesheet" href="assets/css/lity.min.css" />
    <!--====== Main css ======-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <!--====== Responsive css ======-->
    <link rel="stylesheet" href="assets/css/responsive.css" />
</head>

<style>
.page-title-area {
    position: relative;
    z-index: 1;
    margin: 145px 0 8px;
    padding: 75px 0;
    background-size: cover;
    background-position: center;
    background-image: url(assets/img/ban2.jpg) !important;
}

.project-items .project-item .cats {
    margin-top: 35px;
    margin-bottom: 0px;
}

.blog-post-details .post-content {
    background-color: var(--white);
    padding: 35px 26px 80px;
    TEXT-ALIGN: justify;
}

.blog-post-details .post-content .title {
    font-size: 23px;
    margin-bottom: 16px;
    line-height: 1.4;
    text-align: initial;
}

p {
    margin-bottom: 16px;
}

.blog-sidebar .widget.latest-blog-widget li h6 {
    font-size: 12px;
    font-weight: 500;
}
ul.description-list {
    margin: 0px 22px;
    padding: 0px 14px;
    list-style-type: decimal;
}
</style>

<body>
    <!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

    <!--====== Preloader ======-->
    <div id="preloader">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object" id="object_one"></div>
                <div class="object" id="object_two"></div>
                <div class="object" id="object_three"></div>
                <div class="object" id="object_four"></div>
            </div>
        </div>
    </div>

    <!--====== Header Start ======-->
    <?php   include("header.php")?>
    <!--====== Header End ======-->

    <!--====== Page Title Start ======-->
    <section class="page-title-area">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-8">
                    <!-- <h1 class="page-title font-40">What We Do</h1> -->
                </div>
                <div class="col-auto">
                    <ul class="page-breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li>Past News</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--====== Page Title End ======-->

    <!--====== About Section Start ======-->
    <section class="blog-area section-gap-extra-bottom primary-soft-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="blog-post-details">
                        <div class="post-thumbnail">
                            <img src="assets/images/pastnews/TRAIN/1A.jpg" alt="Thumbnail">
                        </div>
                        <div class="post-content">
                            <ul class="post-meta">

                                <li>
                                    <a href="#"><i class="far fa-calendar-alt"></i>22 October 2021</a>
                                </li>
                            </ul>
                            <h3 class="title">
                                Training on Good Agriculture Practices of voluntary Certification Scheme for Medicinal
                                Plants Produce
                            </h3>

                            <p>
                                IMAP, Solidaridad Regional Expertise Centre in association with Quality Council of India
                                organized the one-day capacity building cum training workshop on good agriculture for
                                medicinal plant products on dated 22nd October 2022 and 23rd October 2022 at Ramganj
                                Mandi and Bhavani Madhi respectively. Near about 40 farmers were present on each day of
                                training. </p>
                            <p>
                                The objective of this workshop was to promote good agricultural practices among farmers
                                on medicinal and aromatic plantations. </p>
                            <ul class="description-list">
                                <li>Background and introduction to Voluntary Certification Scheme for Medicinal Plant
                                    Produce (VCSMPP)</li>
                                <li>Standards of good agricultural practices for Medicinal plants in terms of ecological
                                    environment.</li>
                                <li>Diversification of crop for sustainable production of medicinal and aromatic plants.
                                </li>
                                <li>Certification Criteria of Good agricultural practices based on standards under
                                    VCSMPP.</li>
                                <li>Self-assessment by cultivators</li>
                            </ul>

                            <p>
                                The sessions in the consultation were divided into four parts, one inaugural session,
                                and three technical sessions. The first technical session was focused on the background
                                and introduction to voluntary certification schemes for medicinal plants produce, it
                                also includes the current scenario of medicinal plants trade, NMPB’s intervention in
                                augmenting quality and trade, QCI interventions in supporting the genesis of schemes and
                                elements. </p>
                            <p>
                                The experts for the technical sessions were Dr. G.S. Jariyal, Sr. Consultant
                                Solidaridad, Mr. Sarvesh Sharma from QCI and Dr. Saran, Principle Scientist, ICAR, Anand
                                Gujrat. The sessions started by Mr. Sarvesh. </p>
                            <p>
                                Mr. Shivesh Sharma in his session informed about the quality councils of India that QCI
                                established by the Government of India and Indian industries, this organization is
                                functionally autonomous and financially independent. He also shared the regional
                                activities of QCI in terms of international outreach and cooperation such as management
                                of improved agriculture practices and working area (SAARC) and regional trade for
                                sustainable development. During his presentation, he mainly covers the components of the
                                voluntary certification scheme for medicinal plant produce and why VCSMPP is important.

                                </p>
                            <p>
                                <img src="assets/images/pastnews/TRAIN/1B.jpg" alt="Thumbnail">
                                Dr. G S Jariyal, Sr. Consultant, Solidaridad, conducted the second technical session on
                                good agricultural practices in the cultivation of Medicinal and aromatic plants. During
                                his lecture, he throws light on what are the medicinal plants, their uses and current
                                market scenario of MAP. </p>
                            <p>
                                After covering all the above topic Dr. Jariyal, explain the components that have to be
                                known by the farmers while cultivating the medicinal plants such as usage of crops, crop
                                duration, time of sowing, method of sowing, improve variety of seed, numbers of
                                irrigation, diseases and insect on the crop, method of the harvesting, method of grading
                                and proper market of the crop. </p>
                            <p>
                                The last technical session was conducted by Dr. P L saran, Principal Scientist, ICAR,
                                Anand, Gujrat. He started his session with the greetings and introduction of his
                                department and its objective. Dr. Saran majorly focuses on the cost-benefit ratio of the
                                medicinal and aromatic plants as well as processing and packaging of the final good
                                prepared by medicinal or aromatic plants. While explaining the above crops Dr. Saran
                                shared the information about, seed, required climate, management of crop, and components
                                of cost and returns for crops. Apart from this he also discussed about the certification
                                process, benefits of certification, and market for certified farmers. </p>
                            <p>
                                The training programme concluded with a vote of thanks by Dr. Saran to Dr. G S Jariyal,
                                and Mr. Shivesh Sharma for conducting the sessions. He also shared his thanking remark
                                to farmers for showing their interest throughout the workshop. After this, certificates
                                have been distributed to the farmers as an appreciation and further. 
                            </p>
                           <img src="assets/images/pastnews/TRAIN/1C.jpg" alt="Thumbnail">
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="blog-sidebar">

                        <div class="widget latest-blog-widget">
                            <h4 class="widget-title">Past News</h4>
                            <ul>
                                <li>
                                    <div class="thumb">
                                        <img src="assets/images/pastnews/MAND/2.jpg" alt="Image">
                                    </div>
                                    <div class="desc">
                                        <h6><a href="pastnews-details.php">Skill Development Training on Medicinal and
                                                Aromatic Plants-Mandsaur</a>
                                        </h6>
                                        <span class="date"><i class="far fa-calendar-alt"></i>25 May 2021</span>
                                    </div>
                                </li>

                                <li>
                                    <div class="thumb">
                                        <img src="assets/images/pastnews/SEH/1.jpg" alt="Image">
                                    </div>
                                    <div class="desc">
                                        <h6><a href="pastnews-details1.php">Skill Development Training on Sustainable
                                                and Qualitative Production Medicinal Plants-Sehore</a>
                                        </h6>
                                        <span class="date"><i class="far fa-calendar-alt"></i>25 May 2021</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="thumb">
                                        <img src="assets/images/pastnews/TRAIN/A.jpg" alt="Image">
                                    </div>
                                    <div class="desc">
                                        <h6><a href="pastnews-details2.php">Training on Good Agriculture Practices of
                                                voluntary Certification Scheme for Medicinal Plants Produce</a>
                                        </h6>
                                        <span class="date"><i class="far fa-calendar-alt"></i>22 Oct 2021</span>
                                    </div>
                                </li>

                            </ul>
                        </div>

                        <div class="widget cta-widget">
                            <div class="cta-content">
                                <h4 class="title">Become <br> Member</h4>

                                <p>
                                    you can join us and become member by filling this form
                                </p>
                                <a href="IMAP_Membership_Form.pdf" class="cta-button">Join Now</a>


                                <div class="cta-img">
                                    <img src="assets/images/become.jpg" alt="Image">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== About Section End ======-->



    <!--====== Footer Start ======-->
    <?php include("footer.php")?>
    <!--====== Footer End ======-->


    <!--====== jquery js ======-->
    <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <!--====== Bootstrap js ======-->
    <script src="assets/js/bootstrap.min.js"></script>
    <!--====== Inview js ======-->
    <script src="assets/js/jquery.inview.min.js"></script>
    <!--====== Slick js ======-->
    <script src="assets/js/slick.min.js"></script>
    <!--====== Lity js ======-->
    <script src="assets/js/lity.min.js"></script>
    <!--====== Wow js ======-->
    <script src="assets/js/wow.min.js"></script>
    <!--====== Main js ======-->
    <script src="assets/js/main.js"></script>

</body>




</html>