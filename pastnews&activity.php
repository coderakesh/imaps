<!DOCTYPE html>
<html lang="zxx">




<head>
    <!--====== Required meta tags ======-->
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!--====== Title ======-->
    <title> IMAP - Indian Medicinal and Aromatic Plants (I-MAP) - Industry Association || About </title>

    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="img/png" />
    <!--====== Animate Css ======-->
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <!--====== Bootstrap css ======-->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <!--====== Fontawesome css ======-->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
    <!--====== Flaticon css ======-->
    <link rel="stylesheet" href="assets/css/flaticon.css" />
    <!--====== Slick Css ======-->
    <link rel="stylesheet" href="assets/css/slick.min.css" />
    <!--====== Lity Css ======-->
    <link rel="stylesheet" href="assets/css/lity.min.css" />
    <!--====== Main css ======-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <!--====== Responsive css ======-->
    <link rel="stylesheet" href="assets/css/responsive.css" />
</head>

<style>
		.page-title-area {
	position: relative;
	z-index: 1;
	margin: 145px 0 8px;
    padding: 75px 0;
	background-size: cover;
	background-position: center;
	background-image: url(assets/img/ban2.jpg) !important;
}
.project-items .project-item .cats {
    margin-top: 35px;
	margin-bottom: 0px;
}</style>

<body>
    <!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

    <!--====== Preloader ======-->
    <div id="preloader">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object" id="object_one"></div>
                <div class="object" id="object_two"></div>
                <div class="object" id="object_three"></div>
                <div class="object" id="object_four"></div>
            </div>
        </div>
    </div>

    <!--====== Header Start ======-->
    <?php   include("header.php")?>
    <!--====== Header End ======-->

    <!--====== Page Title Start ======-->
    <section class="page-title-area">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-8">
                    <!-- <h1 class="page-title font-40">What We Do</h1> -->
                </div>
                <div class="col-auto">
                    <ul class="page-breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li>Past Events & Activities</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--====== Page Title End ======-->

    <!--====== About Section Start ======-->
    <section class="project-section section-gap-extra-bottom primary-soft-bg">
		<div class="container">
			<div class="row justify-content-center project-items project-style-two">
				<div class="col-xl-10">
					<div class="project-item mb-30">
						<div class="thumb" style="background-image: url(assets/images/pastnewsandactivity/MicrosoftTeams-image.png);"></div>
						<div class="content">
							
							
							<h5 class="title">
								<a href="past-details.php">Trainings of farmers on Good agricultural Practices</a>
							</h5>
							
							<span class="date"><i class="far fa-calendar-alt"></i> 25 February 2021</span>
							<div class="cats">
								<a href="past-details.php">View details</a>
							</div>
						</div>
					</div>
				</div>
                <!-- <div class="col-xl-10">
					<div class="project-item mb-30">
						<div class="thumb" style="background-image: url(assets/img/project/project-list-02.jpg);"></div>
						<div class="content">
							<div class="cats">
								<a href="#">Technology</a>
							</div>
							<div class="author">
								<img src="assets/img/author-thumbs/03.jpg" alt="Thumb">
								<a href="#">James W. Barrows</a>
							</div>
							<h5 class="title">
								<a href="project-details.html">Original Shinecon VR Pro Virtual  Reality 3D Glasses VRBOX</a>
							</h5>
							<div class="project-stats">
								<div class="stats-value">
									<span class="value-title">Raised of <span class="value">$59,689</span></span>
									<span class="stats-percentage">85%</span>
								</div>
								<div class="stats-bar" data-value="85">
									<div class="bar-line"></div>
								</div>
							</div>
							<span class="date"><i class="far fa-calendar-alt"></i> 25 February 2021</span>
						</div>
					</div>
				</div>
				<div class="col-xl-10">
					<div class="project-item mb-30">
						<div class="thumb" style="background-image: url(assets/img/project/project-list-03.jpg);"></div>
						<div class="content">
							<div class="cats">
								<a href="#">Educations</a>
							</div>
							<div class="author">
								<img src="assets/img/author-thumbs/02.jpg" alt="Thumb">
								<a href="#">James W. Barrows</a>
							</div>
							<h5 class="title">
								<a href="project-details.html">Needs Close Up Students Class Room In University</a>
							</h5>
							<div class="project-stats">
								<div class="stats-value">
									<span class="value-title">Raised of <span class="value">$59,689</span></span>
									<span class="stats-percentage">87%</span>
								</div>
								<div class="stats-bar" data-value="87">
									<div class="bar-line"></div>
								</div>
							</div>
							<span class="date"><i class="far fa-calendar-alt"></i> 25 February 2021</span>
						</div>
					</div>
				</div>
                <div class="col-xl-10">
					<div class="project-item mb-30">
						<div class="thumb" style="background-image: url(assets/img/project/project-list-04.jpg);"></div>
						<div class="content">
							<div class="cats">
								<a href="#">Gaming & Tech </a>
							</div>
							<div class="author">
								<img src="assets/img/author-thumbs/01.jpg" alt="Thumb">
								<a href="#">James W. Barrows</a>
							</div>
							<h5 class="title">
								<a href="project-details.html">Fundraising For The People And Causes You Care About</a>
							</h5>
							<div class="project-stats">
								<div class="stats-value">
									<span class="value-title">Raised of <span class="value">$59,689</span></span>
									<span class="stats-percentage">83%</span>
								</div>
								<div class="stats-bar" data-value="83">
									<div class="bar-line"></div>
								</div>
							</div>
							<span class="date"><i class="far fa-calendar-alt"></i> 25 February 2021</span>
						</div>
					</div>
				</div>
				<div class="col-xl-10">
					<div class="project-item mb-30">
						<div class="thumb" style="background-image: url(assets/img/project/project-list-05.jpg);"></div>
						<div class="content">
							<div class="cats">
								<a href="#">Headphone</a>
							</div>
							<div class="author">
								<img src="assets/img/author-thumbs/03.jpg" alt="Thumb">
								<a href="#">James W. Barrows</a>
							</div>
							<h5 class="title">
								<a href="project-details.html">Mobile First Is Just Not Goodies Enough Meet Journey</a>
							</h5>
							<div class="project-stats">
								<div class="stats-value">
									<span class="value-title">Raised of <span class="value">$59,689</span></span>
									<span class="stats-percentage">70%</span>
								</div>
								<div class="stats-bar" data-value="70">
									<div class="bar-line"></div>
								</div>
							</div>
							<span class="date"><i class="far fa-calendar-alt"></i> 25 February 2021</span>
						</div>
					</div>
				</div>
                <div class="col-xl-10">
					<div class="project-item mb-30">
						<div class="thumb" style="background-image: url(assets/img/project/project-list-06.jpg);"></div>
						<div class="content">
							<div class="cats">
								<a href="#">Covid -19</a>
							</div>
							<div class="author">
								<img src="assets/img/author-thumbs/02.jpg" alt="Thumb">
								<a href="#">James W. Barrows</a>
							</div>
							<h5 class="title">
								<a href="project-details.html">COVID-19 Vaccine Have Already Begun Introduced Countries</a>
							</h5>
							<div class="project-stats">
								<div class="stats-value">
									<span class="value-title">Raised of <span class="value">$59,689</span></span>
									<span class="stats-percentage">93%</span>
								</div>
								<div class="stats-bar" data-value="93">
									<div class="bar-line"></div>
								</div>
							</div>
							<span class="date"><i class="far fa-calendar-alt"></i> 25 February 2021</span>
						</div>
					</div>
				</div>
				<div class="col-12">
                    <div class="view-more-btn text-center mt-40">
                        <a href="project-details.html" class="main-btn bordered-btn">View More Project <i class="far fa-arrow-right"></i></a>
                    </div>
                </div> -->
			</div>
		</div>
	</section>
    <!--====== About Section End ======-->



    <!--====== Footer Start ======-->
    <?php include("footer.php")?>
    <!--====== Footer End ======-->


    <!--====== jquery js ======-->
    <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <!--====== Bootstrap js ======-->
    <script src="assets/js/bootstrap.min.js"></script>
    <!--====== Inview js ======-->
    <script src="assets/js/jquery.inview.min.js"></script>
    <!--====== Slick js ======-->
    <script src="assets/js/slick.min.js"></script>
    <!--====== Lity js ======-->
    <script src="assets/js/lity.min.js"></script>
    <!--====== Wow js ======-->
    <script src="assets/js/wow.min.js"></script>
    <!--====== Main js ======-->
    <script src="assets/js/main.js"></script>

</body>




</html>