<!DOCTYPE html>
<html lang="en">


<head>
    <!--====== Required meta tags ======-->
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!--====== Title ======-->
    <title> IMAP - Indian Medicinal and Aromatic Plants (I-MAP) - Industry Association </title>

    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="img/png" />
    <!--====== Animate Css ======-->
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <!--====== Bootstrap css ======-->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <!--====== Fontawesome css ======-->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
    <!--====== Flaticon css ======-->
    <link rel="stylesheet" href="assets/css/flaticon.css" />
    <!--====== Slick Css ======-->
    <link rel="stylesheet" href="assets/css/slick.min.css" />
    <!--====== Lity Css ======-->
    <link rel="stylesheet" href="assets/css/lity.min.css" />
    <!--====== Main css ======-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <!--====== Responsive css ======-->
    <link rel="stylesheet" href="assets/css/responsive.css" />

    <style>
    .page-title-area {
        position: relative;
        z-index: 1;
        margin: 145px 0 8px;
        padding: 75px 0;
        background-size: cover;
        background-position: center;
        background-image: url(assets/img/ban2.jpg) !important;
    }

    body {
        position: initial !important;
    }
    .gallery-item {margin-bottom: 27px;}
    .img-fluid {
   
    width: 100%;
}
    </style>

    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/lightgallery@2.0.0-beta.3/css/lightgallery.css'>
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/lightgallery@2.0.0-beta.3/css/lg-zoom.css'>
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/lightgallery@2.0.0-beta.3/css/lg-fullscreen.css'>
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/lightgallery@2.0.0-beta.3/css/lg-thumbnail.css'>

</head>

<body>
    <!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

    <!--====== Preloader ======-->
    <div id="preloader">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object" id="object_one"></div>
                <div class="object" id="object_two"></div>
                <div class="object" id="object_three"></div>
                <div class="object" id="object_four"></div>
            </div>
        </div>
    </div>

    <!--====== Header Start ======-->
    <?php   include("header.php")?>
    <!--====== Header End ======-->

    <!--====== Page Title Start ======-->
    <section class="page-title-area">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-8">
                    <!-- <h1 class="page-title font-40">Who We Are</h1> -->
                </div>
                <div class="col-auto">
                    <ul class="page-breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li>Gallery</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--====== Page Title End ======-->

    <!--====== About Section Start ======-->
    <section class="about-section-three section-gap ">
        <div class="container">
        <div class="text-center mb-30">
                        <h4><span style="
    padding: 6px 16px  4px 16px;
    border-radius: 5px;
    text-transform: uppercase;
    background-color: #28a745;
    color: white;
">Photo Gallery</span></h4>
                    </div>
            <div class="align-items-center justify-content-center mb-150">

          

                <div class="gallery-container row" id="gallery-container">
                    <a data-lg-size="1400-1400" class="gallery-item col-lg-3"
                        data-src="gallery/big/Photo-1.jpg"
                        data-sub-html="<p>Farmer is observing the condition of Ashwagandha Crop, such as length of roots, condition of seed etc. </p>">
                        <img class="img-fluid"
                            src="gallery/small/photo1.png" />
                    </a>

                    <a data-lg-size="1400-1400" class="gallery-item col-lg-3"
                        data-src="gallery/big/Photo-2.jpg"
                        data-sub-html="<p> Roots of ashwagandha after harvesting </p>">
                        <img class="img-fluid"
                            src="gallery/small/photo2.png" />
                    </a>
                    <a data-lg-size="1400-1400" class="gallery-item col-lg-3"
                        data-src="gallery/big/Photo-3.jpg"
                        data-sub-html="<p> Women Farmer during cleaning of Ashwagandha roots  </p>">
                        <img class="img-fluid"
                            src="gallery/small/photo3.png" />
                    </a>
                    <a data-lg-size="1400-1400" class="gallery-item col-lg-3"
                        data-src="gallery/big/Photo-4.jpg"
                        data-sub-html="<p> Women Farmer during cleaning of Ashwagandha roots  </p>">
                        <img class="img-fluid"
                            src="gallery/small/photo4.png" />
                    </a>
                    <a data-lg-size="1400-1400" class="gallery-item col-lg-3"
                        data-src="gallery/big/Photo-5.jpg"
                        data-sub-html="<p> Women farmer showing roots of ashwagandha after cleaning of roots </p>">
                        <img class="img-fluid"
                            src="gallery/small/photo5.png" />
                    </a>
                    <a data-lg-size="1400-1400" class="gallery-item col-lg-3"
                        data-src="gallery/big/Photo-6.jpg"
                        data-sub-html="<p>Group of farmers with the roots of Ashwagandha after grading </p>">
                        <img class="img-fluid"
                            src="gallery/small/photo6.png" />
                    </a>
                    <a data-lg-size="1400-1400" class="gallery-item col-lg-3"
                        data-src="gallery/big/Photo-7.jpg"
                        data-sub-html="<p>Photo of graded Ashwagandha root in Kalash </p>">
                        <img class="img-fluid"
                            src="gallery/small/photo7.png" />
                    </a>
                    <a data-lg-size="1400-1400" class="gallery-item col-lg-3"
                        data-src="gallery/big/Photo-8.jpg"
                        data-sub-html="<p>Photo of Ashwagandha roots </p>">
                        <img class="img-fluid"
                            src="gallery/small/photo8.png" />
                    </a>
                    <a data-lg-size="1400-1400" class="gallery-item col-lg-3"
                        data-src="gallery/big/Photo-9.jpg"
                        data-sub-html="<p>Photo of Ashwagandha powder </p>">
                        <img class="img-fluid"
                            src="gallery/small/photo9.png" />
                    </a>
                    <a data-lg-size="1400-1400" class="gallery-item col-lg-3"
                        data-src="gallery/big/Photo-10.jpg"
                        data-sub-html="<p>Photo of Ashwagandha powder </p>">
                        <img class="img-fluid"
                            src="gallery/small/photo10.png" />
                    </a>
                    <a data-lg-size="1400-1400" class="gallery-item col-lg-3"
                        data-src="gallery/big/Photo-11.jpg"
                        data-sub-html="<p> Photo of Ashwagandha roots and Powder  </p>">
                        <img class="img-fluid"
                            src="gallery/small/photo11.png" />
                    </a>
                    <a data-lg-size="1400-1400" class="gallery-item col-lg-3"
                        data-src="gallery/big/Photo-12.jpg"
                        data-sub-html="<p> Photo of MAP grower farmers after training of Voluntary Certification Scheme for Medicinal Plant Produce </p>">
                        <img class="img-fluid"
                            src="gallery/small/photo12.png" />
                    </a>
                    <a data-lg-size="1400-1400" class="gallery-item col-lg-3"
                        data-src="gallery/big/Photo-13.jpg"
                        data-sub-html="<p> Photo of MAP grower farmers after training of Voluntary Certification Scheme for Medicinal Plant Produce </p>">
                        <img class="img-fluid"
                            src="gallery/small/photo13.png" />
                    </a>
                    <a data-lg-size="1400-1400" class="gallery-item col-lg-3"
                        data-src="gallery/big/Photo-14.jpg"
                        data-sub-html="<p>Dr. G.S.Jariyal during training session of Voluntary Certification Scheme for Medicinal Plant Produce  </p>">
                        <img class="img-fluid"
                            src="gallery/small/photo14.png" />
                    </a>
                    <a data-lg-size="1400-1400" class="gallery-item col-lg-3"
                        data-src="gallery/big/Photo-15.jpg"
                        data-sub-html="<p> Dr. G.S.Jariyal during training session of Voluntary Certification Scheme for Medicinal Plant Produce </p>">
                        <img class="img-fluid"
                            src="gallery/small/photo15.png" />
                    </a>
                    <a data-lg-size="1400-1400" class="gallery-item col-lg-3"
                        data-src="gallery/big/Photo-16.jpg"
                        data-sub-html="<p>Photo of MAP grower farmers after training of Voluntary Certification Scheme for Medicinal Plant Produce </p>">
                        <img class="img-fluid"
                            src="gallery/small/photo16.png" />
                    </a>
                    <a data-lg-size="1400-1400" class="gallery-item col-lg-3"
                        data-src="gallery/big/Photo-17.jpg"
                        data-sub-html="<p>Photo of farmers group with graded ashwagandha roots  </p>">
                        <img class="img-fluid"
                            src="gallery/small/photo17.png" />
                    </a>
                    <a data-lg-size="1400-1400" class="gallery-item col-lg-3"
                        data-src="gallery/big/Photo-18.jpg"
                        data-sub-html="<p>Shri. Lakhan Pathak, progressive farmer of Solidaridad during harvesting of Ashwagandha roots. </p>">
                        <img class="img-fluid"
                            src="gallery/small/photo18.png" />
                    </a>
                    <a data-lg-size="1400-1400" class="gallery-item col-lg-3"
                        data-src="gallery/big/Photo-19.jpg"
                        data-sub-html="<p>Family of Mr. Lakhan Pathak during grading of ashwagandha roots. </p>">
                        <img class="img-fluid"
                            src="gallery/small/photo19.png" />
                    </a>
                    <a data-lg-size="1400-1400" class="gallery-item col-lg-3"
                        data-src="gallery/big/Photo-20.jpg"
                        data-sub-html="<p>Grinding of Ashwagandha root by Shri Lakhan Pathak and his spouse.</p>">
                        <img class="img-fluid"
                            src="gallery/small/photo20.png" />
                    </a>
                    <a data-lg-size="1400-1400" class="gallery-item col-lg-3"
                        data-src="gallery/big/Photo-21.jpg"
                        data-sub-html="<p>Skill development training of farmers on MAP </p>">
                        <img class="img-fluid"
                            src="gallery/small/photo21.png" />
                    </a>
                    <a data-lg-size="1400-1400" class="gallery-item col-lg-3"
                        data-src="gallery/big/Photo-22.jpg"
                        data-sub-html="<p>Exposure visits of farmers after skill development training, Mandsaur </p>">
                        <img class="img-fluid"
                            src="gallery/small/photo22.png" />
                    </a>
                    <a data-lg-size="1400-1400" class="gallery-item col-lg-3"
                        data-src="gallery/big/Photo-23.jpg"
                        data-sub-html="<p>Exposure visits of farmers after skill development training, Sehore </p>">
                        <img class="img-fluid"
                            src="gallery/small/photo23.png" />
                    </a>
                </div>





            </div>
        </div>
    </section>
    <!--====== About Section End ======-->

    <!-- <div class="gallery-container " >

<a data-lg-size="1400-1400" class="gallery-item" src="https://images.unsplash.com/photo-1588093413519-17cec3f64e40?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1400&q=80" data-sub-html="<h4>Photo by - <a href='https://unsplash.com/@entrycube' >Diego Guzmán </a></h4> <p> Location - <a href='https://unsplash.com/s/photos/fushimi-inari-taisha-shrine-senbontorii%2C-68%E7%95%AA%E5%9C%B0-fukakusa-yabunouchicho%2C-fushimi-ward%2C-kyoto%2C-japan'>Fushimi Ward, Kyoto, Japan</a></p>">
  <img class="img-fluid" src="https://images.unsplash.com/photo-1588093413519-17cec3f64e40?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=240&q=80" />
</a>
<a data-lg-size="1400-1400" class="gallery-item" data-src="https://images.unsplash.com/photo-1563502310703-1ffe473ad66d?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1443&q=80" data-sub-html="<h4>Photo by - <a href='https://unsplash.com/@asoshiation' >Shah </a></h4><p> Location - <a href='https://unsplash.com/s/photos/shinimamiya%2C-osaka%2C-japan'>Shinimamiya, Osaka, Japan</a></p>">
  <img class="img-fluid" src="https://images.unsplash.com/photo-1563502310703-1ffe473ad66d?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=240&q=80" />
</a>
<a data-lg-size="1400-1400" class="gallery-item" data-src="https://images.unsplash.com/photo-1613541444699-39429d990353?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1400&q=80" data-sub-html="<h4>Photo by - <a href='https://unsplash.com/@katherine_xx11' >Katherine Gu </a></h4><p> For all those years we were alone and helpless.</p>">
  <img class="img-fluid" src="https://images.unsplash.com/photo-1613541444699-39429d990353?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=240&q=80" />
</a>
</div> -->

    <!--====== Footer Start ======-->
    <?php include("footer.php")?>
    <!--====== Footer End ======-->


    <!--====== jquery js ======-->
    </script>
    <script src="assets/js/jquery.min.js"></script>
    <!--====== Bootstrap js ======-->
    <script src="assets/js/bootstrap.min.js"></script>
    <!--====== Inview js ======-->
    <script src="assets/js/jquery.inview.min.js"></script>
    <!--====== Slick js ======-->
    <script src="assets/js/slick.min.js"></script>
    <!--====== Lity js ======-->
    <script src="assets/js/lity.min.js"></script>
    <!--====== Wow js ======-->
    <script src="assets/js/wow.min.js"></script>
    <!--====== Main js ======-->
    <script src="assets/js/main.js"></script>
    <script src='https://cdn.jsdelivr.net/npm/lightgallery@2.0.0-beta.3/lightgallery.umd.js'></script>
    <script src='https://cdn.jsdelivr.net/npm/lightgallery@2.0.0-beta.3/plugins/zoom/lg-zoom.umd.js'></script>
    <script src='https://cdn.jsdelivr.net/npm/lightgallery@2.0.0-beta.3/plugins/fullscreen/lg-fullscreen.umd.js'>
    </script>
    <script src='https://cdn.jsdelivr.net/npm/lightgallery@2.0.0-beta.3/plugins/thumbnail/lg-thumbnail.umd.js'></script>







    <script src="assets/js/script.js"></script>

</body>

</html>