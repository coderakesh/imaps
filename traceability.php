<!DOCTYPE html>
<html lang="zxx">




<head>
    <!--====== Required meta tags ======-->
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!--====== Title ======-->
    <title> IMAP - Indian Medicinal and Aromatic Plants (I-MAP) - Industry Association || About </title>

    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="img/png" />
    <!--====== Animate Css ======-->
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <!--====== Bootstrap css ======-->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <!--====== Fontawesome css ======-->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
    <!--====== Flaticon css ======-->
    <link rel="stylesheet" href="assets/css/flaticon.css" />
    <!--====== Slick Css ======-->
    <link rel="stylesheet" href="assets/css/slick.min.css" />
    <!--====== Lity Css ======-->
    <link rel="stylesheet" href="assets/css/lity.min.css" />
    <!--====== Main css ======-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <!--====== Responsive css ======-->
    <link rel="stylesheet" href="assets/css/responsive.css" />
</head>

<style>
.page-title-area {
    position: relative;
    z-index: 1;
    margin: 145px 0 8px;
    padding: 75px 0;
    background-size: cover;
    background-position: center;
    background-image: url(assets/img/ban2.jpg) !important;
}

.project-items .project-item .cats {
    margin-top: 35px;
    margin-bottom: 0px;
}

.blog-post-details .post-content {
    background-color: var(--white);
    padding: 35px 26px 80px;
    TEXT-ALIGN: justify;
}

.section-gap {
    padding-bottom: 116px;
}

.common-heading .title {
    font-size: 29px;
}
.about-list li {
    position: relative;
    padding-left: 50px;
    font-weight: 800;
}
</style>

<body>
    <!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

    <!--====== Preloader ======-->
    <div id="preloader">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object" id="object_one"></div>
                <div class="object" id="object_two"></div>
                <div class="object" id="object_three"></div>
                <div class="object" id="object_four"></div>
            </div>
        </div>
    </div>

    <!--====== Header Start ======-->
    <?php   include("header.php")?>
    <!--====== Header End ======-->

    <!--====== Page Title Start ======-->
    <section class="page-title-area">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-8">
                    <!-- <h1 class="page-title font-40">What We Do</h1> -->
                </div>
                <div class="col-auto">
                    <ul class="page-breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li>Traceability</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--====== Page Title End ======-->

    <!--====== About Section Start ======-->
    <section class="about-section-three section-gap">
        <div class="container">
            <div class="row  justify-content-center">
                <div class="col-lg-4 ">
                    <div class="about-gallery wow fadeInRight">
                        <div class="img-one">
                            <img src="trace.png" alt="Image">
                        </div>

                    </div>
                </div>
                <div class="col-lg-8 ">
                    <div class="about-text mb-lg-50">
                        <div class="common-heading mb-10">


                            <h2 class="title">Traceability of medicinal farm produce</h2>
                        </div>
                        <p class="mb-20">
                            In Ayurveda, extracts or powder of medicine etc are derived from medicinal crops that
                            provides both nutrition and medicinal benefits. They could be referred to as medicinal or
                            herbals supplements. Ashwagandha and curcumin are the leading contributors to the
                            manufacture of nutraceuticals. In the supply chain of the medicinal and aromatic plants
                            complete transparency is required as consumers have the right to know the farm to bottle
                            story of the products so that they can make informed choices of buying the products.
                        </p>
                        <p>
                            By considering the same traceability app will be developed so that consumers would be able
                            to view the producers and hear from them directly, and learn about the safety and quality
                            standards.
                        </p>
                    </div>


                    </div>
                    <div class="col-lg-12 ">
                        <div class="content mt-30">
                            <h4 class="title text-center">Why Traceability in Medicinal plants are required? </h4>

                        </div>
                        <ul class="about-list mt-30">
                            <li> Changing Market Dynamics: </li>
                            <p>
                                With the rising global concerns about climate change, consumers are growing conscious of
                                the social and ecological footprints of human activities. They want to know the
                                provenance of the products and their traceability along the agricultural supply chain.
                            </p>
                            <li> The Global Urgency: </li>
                            <p>Companies are under pressure to ensure foolproof sustainable practices across their
                                supply chain. Regulatory bodies worldwide are looking to adopt stringent policies, some
                                of the countries had passing a law to impose penalties on businesses for any breach of
                                human rights or environmental rules.</p>
                            <li> The Importance of Food Safety: </li>
                            <p>Companies are under pressure to ensure foolproof sustainable practices across their
                                supply chain. Regulatory bodies worldwide are looking to adopt stringent policies, some
                                of the countries had passing a law to impose penalties on businesses for any breach of
                                human rights or environmental rules.</p>
                                <p> These trends necessitate a powerful technology backed mechanism to trace the sustainability practices across the agriculture supply channels. Such platforms must ideally include hardware, software, digital tracking system and on ground farmers. </p>
                        </ul>
                        <div class="content mt-30">
                            <h4 class="title">How Traceability app will work?  </h4>

                        </div>
                        <ul class="about-list mt-30">
                          
                            <p>
                            Traceability app for the produce of medicinal and aromatic plants will play an important role in the supply value chain, it will create a win-win situation for all the stakeholders. This will bring them into a strong position to get best return on their investment and incentive for their sustainability commitment. 
                            </p>
                            <li> It will create the data base for entire process of supply chain </li>
                            <p>Data will be maintained from sowing methods, variety, sustainable agriculture practices performed by farmers, intermediaries who transport and agri-business who deliver finished products to the consumer market. </p>
                            <li> It will make consumers aware about the product story  </li>
                            <p>QR code will be provided on the packages of the products, by scanning the QR code consumers will be able to access the details of the product’s journey, addition to that consumer will be able to send the feedback to the farmers, it will help in establishing a strong human connection for goodwill and brand loyalty. </p>
                               
                        </ul> </div>






                </div>
            </div>


            <div class="container-fluid text-center mb-20">   </div>


<h4 class="mt-30 text-center">Supply chain and Traceability of products prepared from Medicinal and Aromatic</h4>

<img src="traceimg.png" style="
    margin: 0px 0px 50px;
" alt="Image">


        </div>
    </section>
    <!--====== About Section End ======-->



    <!--====== Footer Start ======-->
    <?php include("footer.php")?>
    <!--====== Footer End ======-->


    <!--====== jquery js ======-->
    <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <!--====== Bootstrap js ======-->
    <script src="assets/js/bootstrap.min.js"></script>
    <!--====== Inview js ======-->
    <script src="assets/js/jquery.inview.min.js"></script>
    <!--====== Slick js ======-->
    <script src="assets/js/slick.min.js"></script>
    <!--====== Lity js ======-->
    <script src="assets/js/lity.min.js"></script>
    <!--====== Wow js ======-->
    <script src="assets/js/wow.min.js"></script>
    <!--====== Main js ======-->
    <script src="assets/js/main.js"></script>

</body>




</html>