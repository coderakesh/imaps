<!DOCTYPE html>
<html lang="zxx">




<head>
    <!--====== Required meta tags ======-->
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!--====== Title ======-->
    <title> IMAP - Indian Medicinal and Aromatic Plants (I-MAP) - Industry Association </title>

    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="img/png" />
    <!--====== Animate Css ======-->
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <!--====== Bootstrap css ======-->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <!--====== Fontawesome css ======-->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
    <!--====== Flaticon css ======-->
    <link rel="stylesheet" href="assets/css/flaticon.css" />
    <!--====== Slick Css ======-->
    <link rel="stylesheet" href="assets/css/slick.min.css" />
    <!--====== Lity Css ======-->
    <link rel="stylesheet" href="assets/css/lity.min.css" />
    <!--====== Main css ======-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <!--====== Responsive css ======-->
    <link rel="stylesheet" href="assets/css/responsive.css" />
</head>

<style>
.page-title-area {
    position: relative;
    z-index: 1;
    margin: 145px 0 8px;
    padding: 75px 0;
    background-size: cover;
    background-position: center;
    background-image: url(assets/img/ban2.jpg) !important;
}

.project-items .project-item .cats {
    margin-top: 35px;
    margin-bottom: 0px;
}

.blog-post-details .post-content {
    background-color: var(--white);
    padding: 35px 26px 80px;
    TEXT-ALIGN: justify;
}

.blog-post-details .post-content .title {
    font-size: 23px;
    margin-bottom: 16px;
    line-height: 1.4;
    text-align: initial;
}

p {
    margin-bottom: 16px;
}

.blog-sidebar .widget.latest-blog-widget li h6 {
    font-size: 12px;
    font-weight: 500;
}
</style>

<body>
    <!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

    <!--====== Preloader ======-->
    <div id="preloader">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object" id="object_one"></div>
                <div class="object" id="object_two"></div>
                <div class="object" id="object_three"></div>
                <div class="object" id="object_four"></div>
            </div>
        </div>
    </div>

    <!--====== Header Start ======-->
    <?php   include("header.php")?>
    <!--====== Header End ======-->

    <!--====== Page Title Start ======-->
    <section class="page-title-area">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-8">
                    <!-- <h1 class="page-title font-40">What We Do</h1> -->
                </div>
                <div class="col-auto">
                    <ul class="page-breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li>Past News</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--====== Page Title End ======-->

    <!--====== About Section Start ======-->
    <section class="blog-area section-gap-extra-bottom primary-soft-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="blog-post-details">
                        <div class="post-thumbnail">
                            <img src="assets/images/activity/1a.jpg" alt="Thumbnail">
                        </div>
                        <div class="post-content">
                            <ul class="post-meta">

                                <li>
                                    <a href="#"><i class="far fa-calendar-alt"></i>25 February 2021</a>
                                </li>
                            </ul>
                            <h3 class="title">
                                From Low-value Crops To High-value Market-oriented Maps
                            </h3>

                            <p>
                                The cultivation of the MAPs is also fraught with the perils of substitutes and
                                adulterants traded freely in the market, thus affecting the quality and efficacy of the
                                end product. In addition, issues around accurate identification of species and variation
                                in quality plague the sector. The largely unregulated and non-transparent supply chain
                                contributes to the various constraints restricting the growth of the herbal sector in
                                India. The farmers' lack of access to technical knowhows and advisory support for
                                cultivation and management of MAPs adds to the issue as they often land up purchasing
                                poor-quality planting materials from various sellers and at a very high price.
                                Solidaridad's intervention is focused on reclaiming sustainability in the medicinal and
                                aromatic plants and herbal medicine sector through an integrated market-based and
                                entrepreneurial approach. Toeing its multi-annual strategic plan, Solidaridad is working
                                on a four-level intervention area: educating farmers with scientific knowhows and good
                                agricultural practices; supporting them in developing a viable business ecosystem;
                                enabling an inclusive policy environment; and creating a market for affordable and
                                sustainable products.
                                
                                
                            </p>



                            <p>The MAPs are identified as promising crops for crop diversification, which can turn
                                agriculture into being more remunerative. Crop diversification also builds farmers'
                                resilience towards changing climate and associated risks of crop failure. Small holder
                                farmers are the most vulnerable to climate change. Extreme weather events in the last
                                few years have brought losses to traditional crops in Madhya Pradesh and Rajasthan.
                                Diversifying farmland for cultivation of different crops therefore acts as a buffer
                                during these times.
                            </p>
                            <p>
                            The demand for medicinal plants is growing worldwide and has the potential to fetch good
                                income for the farmers compared to that of traditional crops. The programme is
                                encouraging small farmers to implement a shift of resources in the cultivated area, from
                                cereals and low-value crops to high-value medicinal and aromatic plants. These
                                high-value plants require less inputs and hence relate to better income realisation for
                                farmers along with facilitating their production in balance with nature.
                            </p>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="blog-sidebar">

                        <div class="widget latest-blog-widget">
                            <h4 class="widget-title">Activity</h4>
                            <ul>
                                <li>
                                    <div class="thumb">
                                        <img src="assets/images/activity/4a.jpg" alt="Image">
                                    </div>
                                    <div class="desc">
                                        <h6><a href="upcomingevents-details.php">The Future Of High-value Medicinal &
                                                Aromatic
                                                Plants</a>
                                        </h6>
                                        <span class="date"><i class="far fa-calendar-alt"></i>25 May 2021</span>
                                    </div>
                                </li>

                                <li>
                                    <div class="thumb">
                                        <img src="assets/images/activity/1.jpg" alt="Image">
                                    </div>
                                    <div class="desc">
                                        <h6><a href="upcomingevents-details1.php">From Low-value Crops To High-value
                                                Market-oriented Maps</a>
                                        </h6>
                                        <span class="date"><i class="far fa-calendar-alt"></i>25 May 2021</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="thumb">
                                        <img src="assets/images/activity/5.jpg" alt="Image">
                                    </div>
                                    <div class="desc">
                                        <h6><a href="upcomingevents-details2.php">Rising Income And Livelihood
                                                Conditions</a>
                                        </h6>
                                        <span class="date"><i class="far fa-calendar-alt"></i>22 Oct 2021</span>
                                    </div>
                                </li>

                                <li>
                                    <div class="thumb">
                                        <img src="assets/images/activity/2.jpg" alt="Image">
                                    </div>
                                    <div class="desc">
                                        <h6><a href="upcomingevents-details3.php">Enabling Market Access And
                                                Entrepreneurship</a>
                                        </h6>
                                        <span class="date"><i class="far fa-calendar-alt"></i>22 Oct 2021</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="thumb">
                                        <img src="assets/images/activity/3.jpg" alt="Image">
                                    </div>
                                    <div class="desc">
                                        <h6><a href="upcomingevents-details4.php">Towards Prosperity, Inclusivity And
                                                Balanced
                                                Production</a>
                                        </h6>
                                        <span class="date"><i class="far fa-calendar-alt"></i>22 Oct 2021</span>
                                    </div>
                                </li>


                            </ul>
                        </div>

                        <div class="widget cta-widget">
                            <div class="cta-content">
                                <h4 class="title">Become <br> Member</h4>

                                <p>
                                    you can join us and become member by filling this form
                                </p>
                                <a href="IMAP_Membership_Form.pdf" class="cta-button">Join Now</a>


                                <div class="cta-img">
                                    <img src="assets/images/become.jpg" alt="Image">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== About Section End ======-->



    <!--====== Footer Start ======-->
    <?php include("footer.php")?>
    <!--====== Footer End ======-->


    <!--====== jquery js ======-->
    <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <!--====== Bootstrap js ======-->
    <script src="assets/js/bootstrap.min.js"></script>
    <!--====== Inview js ======-->
    <script src="assets/js/jquery.inview.min.js"></script>
    <!--====== Slick js ======-->
    <script src="assets/js/slick.min.js"></script>
    <!--====== Lity js ======-->
    <script src="assets/js/lity.min.js"></script>
    <!--====== Wow js ======-->
    <script src="assets/js/wow.min.js"></script>
    <!--====== Main js ======-->
    <script src="assets/js/main.js"></script>

</body>




</html>