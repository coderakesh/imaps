<!DOCTYPE html>
<html lang="en">

<head>
    <!--====== Required meta tags ======-->
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!--====== Title ======-->
    <title> IMAP - Indian Medicinal and Aromatic Plants (I-MAP) - Industry Association || Home </title>
    <!--====== Favicon Icon ======-->
    <?php   include("link.php")?>
</head>

<body>
    <!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->
    <!--====== Preloader ======-->
    <div id="preloader">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object" id="object_one"></div>
                <div class="object" id="object_two"></div>
                <div class="object" id="object_three"></div>
                <div class="object" id="object_four"></div>
            </div>
        </div>
    </div>
    <!--====== Header Start ======-->
    <?php   include("header.php")?>
    <!--====== Header End ======-->
    <!--====== Hero Area Start ======-->
    <section class="hero-area-two"><?php   include("slider.php")?>
        <div class="container">
        </div>
        <div class="hero-shapes">
            <div class="hero-line-one">
                <!-- <img src="assets/img/hero/hero-line-3.png" alt="Line"> -->
            </div>
            <div class="hero-line-two">
                <img src="assets/img/hero/hero-line-2.png" alt="Line">
            </div>
            <div class="dot-one"></div>
            <div class="dot-two"></div>
        </div>
    </section>
    <!--====== Hero Area End ======-->
    <!--====== About Section Start ======-->
    <section class="about-section-three section-gap">
        <div class="container">
            <div class="row  justify-content-center">
                <div class="col-xl-6 col-lg-6 col-md-10">
                    <div class="about-gallery wow fadeInRight">
                        <div class="img-one">
                            <img src="assets/img/about/about-1.jpg" alt="Image">
                        </div>
                        <div class="img-two wow fadeInUp">
                            <img src="assets/img/about/about-2.png" alt="Image">
                        </div>
                        <div class="pattern">
                            <img src="assets/img/about/about-gallery-pattern.png" alt="Pattern">
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-9 col-sm-10">
                    <div class="about-text mb-lg-50">
                        <div class="common-heading mb-10">
                            <span class="tagline">
                                <i class="fas fa-plus"></i> About us
                                <!-- <span class="heading-shadow-text">About Us</span> -->
                            </span>

                            <h2 class="title1">Who <span class="highlighter">We</span> Are</h2>
                        </div>
                        <p class="mb-30">
                            <b> Medicinal and Aromatic Plants (MAPs)</b> sector significantly contribute towards the
                            global,
                            national and local economies. <b>MAPs</b> constitute a crucial resource for a wide scale of
                            products, varying from medicines, to food supplements, superfoods, food ingredients,
                            cosmetics and body care products etc. Worldwide recent consumer trends have boosted the
                            demand for natural ingredients as consumers started preferring natural over synthetic.
                            India being the key player in the <b> MAPs </b> sector has an important role to play in
                            promotion of
                            sustainability in the sector. Through the <b>Indian Medicinal and Aromatic Plants (I-MAP) -
                                Industry Association,</b> our endeavour is to work with the stakeholders like
                            Government,
                            farmers, collectors,....
                        </p>
                        <a href="who-we-are.php" class="theme-btn">Read More<i class="fa fa-angle-double-right"
                                aria-hidden="true"></i></a>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== About Section End ======-->
    <!--====== Project Section Start ======-->
    <section class="project-section project-section-two">
        <div class="container fluid-extra-padding">
            <div class="common-heading text-center color-version-white mb-30">
                <span class="tagline">
                    <i class="fas fa-plus"></i> News & Events
                </span>
            </div>
            <div class="row align-items-center justify-content-center">
                <div class="event-items col-xl-6 col-lg-6 col-md-6 col-sm-6">
                    <div class="common-heading text-center color-version-white mb-30">
                        <h2 class="title"><span class="highlighter">News</span> & Updates</h2>
                    </div>
                    <div class="single-event-item mb-30 wow fadeInUp" data-wow-delay="0s">
                        <div class="event-thumb">
                            <img src="assets/img/event/1.png" alt="Image">
                        </div>
                        <div class="event-content">
                            <ul class="meta  mb-0 ">
                                <li>
                                    <a href="#." class="category center-with-width">Nov
                                        <span>28</span></a>
                                </li>
                                <li class="visible-xs">
                                    <h4 class="event-title"><a href="#">Combining UX Design &amp; Psychology for user
                                            experience</a>
                                    </h4>
                                    <a href="#" class="reply-link mt-4">Read More <i class="far fa-chevron-double-right"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="event-content hidden-xs">
                            <h4 class="event-title"><a href="#">Combining UX Design & Psychology for user experience</a>
                            </h4>
                            <a href="#" class="reply-link mt-4">Read More <i
                                    class="far fa-chevron-double-right"></i></a>
                        </div>
                    </div>
                    <div class="single-event-item mb-30 wow fadeInUp" data-wow-delay="0.1s">
                        <div class="event-thumb">
                            <img src="assets/img/event/2.png" alt="Image">
                        </div>
                        <div class="event-content">
                            <ul class="meta  mb-0">
                                <li>
                                    <a href="#." class="category center-with-width">Nov
                                        <span>28</span></a>
                                </li>
                                <li class="visible-xs">
                                    <h4 class="event-title"><a href="#">Combining UX Design &amp; Psychology for user
                                            experience</a>
                                    </h4>
                                    <a href="#" class="reply-link mt-4">Read More <i class="far fa-chevron-double-right"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="event-content hidden-xs">
                            <h4 class="event-title"><a href="#">Combining UX Design & Psychology for user experience</a>
                            </h4>
                            <a href="#" class="reply-link mt-4">Read More <i
                                    class="far fa-chevron-double-right"></i></a>
                        </div>
                    </div>
                    <div class="view-more-btn text-center mt-30 mb-20">
                        <a href="pastnews.php" class="theme-btn">More News<i class="fa fa-angle-double-right"
                                aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="event-items col-xl-6 col-lg-6 col-md-6 col-sm-6">

                    <div class="common-heading text-center color-version-white mb-30">
                        <h2 class="title">Upcoming <span class="highlighter">Events</span> </h2>
                    </div>
                    <div class="single-event-item mb-30 wow fadeInUp" data-wow-delay="0s">
                        <div class="event-thumb">
                            <img src="assets/img/event/3.jpg" alt="Image">
                        </div>
                        <div class="event-content">
                            <h4 class="event-title"><a href="#">Combining UX Design</a></h4>
                            <ul class="meta">
                                <li>
                                    <a href="#." class="category ">December 20,2021 </a>
                                </li>
                            </ul>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error voluptatem accus laudantium totam rem
                                aperiam eaque
                            </p>
                            <a href="#" class="reply-link">Read More <i class="far fa-chevron-double-right"></i></a>
                        </div>
                    </div>
                    <div class="single-event-item mb-30 wow fadeInUp" data-wow-delay="0.1s">
                        <div class="event-thumb">
                            <img src="assets/img/event/4.png" alt="Image">
                        </div>
                        <div class="event-content">
                            <h4 class="event-title"><a href="#">Combining UX Design</a></h4>
                            <ul class="meta">
                                <li>
                                    <a href="#." class="category ">December 20,2021 </a>
                                </li>
                            </ul>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error voluptatem accus laudantium totam rem
                                aperiam eaque
                            </p>
                            <a href="#" class="reply-link">Read More <i class="far fa-chevron-double-right"></i></a>
                        </div>
                    </div>
                    <div class="view-more-btn text-center mt-30 mb-20">
                    <a href="upcomingevents&activity.php" class="theme-btn">More Events<i class="fa fa-angle-double-right"
                                aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== Project Section End ======-->
    <!--====== Objectives ======-->
    <section class="about-section-three section-gap-extra-bottom">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-xl-7 col-lg-7 col-md-9 col-sm-10">
                    <div class="about-text mb-lg-50">
                        <div class="common-heading mb-30">
                            <span class="tagline">
                                <i class="fas fa-plus"></i> Objectives 
                                <!-- <span class="heading-shadow-text">About Us</span> -->
                            </span>
                            <h2 class="title">Objectives of <span class="highlighter">I-MAP</span></h2>
                        </div>
                        <p>
                        I-MAP act as a key contact point for the medicinal plants industry stakeholders with regard to guidance, interpretation, monitoring and support in implementation of sustainable supply chain initiatives.
                        </p>
						<ul class="about-list mt-25">
							<li>Provide support for initiatives around sustainable production and consumption of medicinal and aromatic plants </li>
							<li>Provide a platform for exchange of knowledge and ideas related to medicinal and aromatic plants sector as well as communication and dissemination of value-added information, sector updates and newsletters etc.</li>
							<li>Foster cooperation, collective and coordinated efforts by sector stakeholders for the advancement and long-term sustainability of the sector </li>
							<li>Advocacy of sector’s interests and enabling policies that can work at national, regional and local level and facilitate sustainable growth of sector </li> 
						</ul>
                        <a href="#" class="theme-btn mt-25">Learn More<i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-5 col-md-10">
                    <div class="about-gallery pl-0 wow fadeInRight">
                        <div class="img-one">
                            <img src="assets/img/objective.png" alt="Image">
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== Objectives ======-->
    <!--====== Footer Start ======-->
    <?php include("footer.php")?>
    <!--====== Footer End ======-->
    <!--====== jquery js ======-->
    <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <!--====== Bootstrap js ======-->
    <script src="assets/js/bootstrap.min.js"></script>
    <!--====== Inview js ======-->
    <script src="assets/js/jquery.inview.min.js"></script>
    <!--====== Slick js ======-->
    <script src="assets/js/slick.min.js"></script>
    <!--====== Lity js ======-->
    <script src="assets/js/lity.min.js"></script>
    <!--====== Wow js ======-->
    <script src="assets/js/wow.min.js"></script>
    <!--====== Main js ======-->
    <script src="assets/js/main.js"></script>
    <script type="text/javascript" src="dist/owl.carousel.js"></script>
    <script>
    $('.owl-carousel').owlCarousel({
        items: 1,
        lazyLoad: true,
        autoplay: true,
        loop: true,
        dots: false,
        nav: false
    });
    </script>
</body>

</html>