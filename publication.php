<!DOCTYPE html>
<html lang="zxx">




<head>
    <!--====== Required meta tags ======-->
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!--====== Title ======-->
    <title> IMAP - Indian Medicinal and Aromatic Plants (I-MAP) - Industry Association || About </title>

    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="img/png" />
    <!--====== Animate Css ======-->
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <!--====== Bootstrap css ======-->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <!--====== Fontawesome css ======-->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
    <!--====== Flaticon css ======-->
    <link rel="stylesheet" href="assets/css/flaticon.css" />
    <!--====== Slick Css ======-->
    <link rel="stylesheet" href="assets/css/slick.min.css" />
    <!--====== Lity Css ======-->
    <link rel="stylesheet" href="assets/css/lity.min.css" />
    <!--====== Main css ======-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <!--====== Responsive css ======-->
    <link rel="stylesheet" href="assets/css/responsive.css" />
</head>

<style>
		.page-title-area {
	position: relative;
	z-index: 1;
	margin: 145px 0 8px;
    padding: 75px 0;
	background-size: cover;
	background-position: center;
	background-image: url(assets/img/ban2.jpg) !important;
}
.project-items .project-item .cats {
    margin-top: 35px;
	margin-bottom: 0px;
}


.event-items .single-event-item .event-thumb img {
    width: 192px;
    height: 192px;
    border-radius: 50%;
}


.main-btn.bordered-btn {
    background-color: #ffc30e;
    border-color: var(--border-color);
    color: #0e0e0e;
}



</style>

<body>
    <!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

    <!--====== Preloader ======-->
    <div id="preloader">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object" id="object_one"></div>
                <div class="object" id="object_two"></div>
                <div class="object" id="object_three"></div>
                <div class="object" id="object_four"></div>
            </div>
        </div>
    </div>

    <!--====== Header Start ======-->
    <?php   include("header.php")?>
    <!--====== Header End ======-->

    <!--====== Page Title Start ======-->
    <section class="page-title-area">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-8">
                    <!-- <h1 class="page-title font-40">What We Do</h1> -->
                </div>
                <div class="col-auto">
                    <ul class="page-breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li>Publication</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--====== Page Title End ======-->

    <!--====== About Section Start ======-->
    <section class="event-area section-gap-extra-bottom">
		<div class="container">
            <div class="event-items">
                <div class="single-event-item mb-30 wow fadeInUp" data-wow-delay="0s">
                    <div class="event-thumb">
                        <img src="assets/images/publication/1.jpg" alt="Image">
                    </div>
                    <div class="event-content "  style="width:100%">
                        <!-- <ul class="meta">
                            <li>
                                <a href="project-details.html" class="category">Music Party</a>
                            </li>
                            <li>
                                <a href="#" class="date"><i class="fal fa-map-marker-alt"></i>25 Main Street ,New York</a>
                            </li>
                        </ul> <ul class="meta">
                           
                            <li>
                                <a href="#" class="date"><i class="fal fa-map-marker-alt"></i>Sept 2021</a>
                            </li>
                        </ul> -->

                       
                        <h4 class="event-title"><a href="#">Herbal Home Garden</a></h4>
                        <p>
                        Author : Solidaridad Regional Expertise Centre 
                        </p>
                    </div>
                    <div class="event-button">
                        <a href="assets/images/publication/Herbal Home Garden_1.pdf" class="main-btn bordered-btn">View Publication <i class="far fa-arrow-right"></i></a>
                    </div>
                </div>
                <div class="single-event-item mb-30 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="event-thumb">
                        <img src="assets/images/publication/2.jpg" alt="Image">
                    </div>
                    <div class="event-content">
                       
                        <h4 class="event-title"><a href="#">Reclaiming Sustainability in Medicinal Plants and erbal Medicine Sector</a></h4>
                        <p>
                        Author : Dr. Gurpal Singh Jarial , Dr. Suresh Motwani Himanshu Bais  
                        </p>
                    </div>
                    <div class="event-button">
                        <a href="assets/images/publication/Reclaiming Sustainability in Medicinal Plants and erbal Medicine Sector 1.pdf" class="main-btn bordered-btn">View Publication <i class="far fa-arrow-right"></i></a>
                    </div>
                </div>
           
              
                <!-- <div class="view-more-btn text-center mt-80">
                    <a href="project-1.html" class="main-btn bordered-btn">View More Events <i class="far fa-arrow-right"></i></a>
                </div> -->
            </div>
		</div>
	</section>
    <!--====== About Section End ======-->



    <!--====== Footer Start ======-->
    <?php include("footer.php")?>
    <!--====== Footer End ======-->


    <!--====== jquery js ======-->
    <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <!--====== Bootstrap js ======-->
    <script src="assets/js/bootstrap.min.js"></script>
    <!--====== Inview js ======-->
    <script src="assets/js/jquery.inview.min.js"></script>
    <!--====== Slick js ======-->
    <script src="assets/js/slick.min.js"></script>
    <!--====== Lity js ======-->
    <script src="assets/js/lity.min.js"></script>
    <!--====== Wow js ======-->
    <script src="assets/js/wow.min.js"></script>
    <!--====== Main js ======-->
    <script src="assets/js/main.js"></script>

</body>




</html>