<!DOCTYPE html>
<html lang="zxx">


<head>
	<!--====== Required meta tags ======-->
	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge" />
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<!--====== Title ======-->
	<title> IMAP - Indian Medicinal and Aromatic Plants (I-MAP) - Industry Association  </title>

	<!--====== Favicon Icon ======-->
	<link rel="shortcut icon" href="assets/img/favicon.ico" type="img/png" />
	<!--====== Animate Css ======-->
	<link rel="stylesheet" href="assets/css/animate.min.css">
	<!--====== Bootstrap css ======-->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
	<!--====== Fontawesome css ======-->
	<link rel="stylesheet" href="assets/css/font-awesome.min.css" />
	<!--====== Flaticon css ======-->
	<link rel="stylesheet" href="assets/css/flaticon.css" />
	<!--====== Slick Css ======-->
	<link rel="stylesheet" href="assets/css/slick.min.css" />
	<!--====== Lity Css ======-->
	<link rel="stylesheet" href="assets/css/lity.min.css" />
	<!--====== Main css ======-->
	<link rel="stylesheet" href="assets/css/main.css" />
	<!--====== Responsive css ======-->
	<link rel="stylesheet" href="assets/css/responsive.css" />

	<style>
		.page-title-area {
	position: relative;
	z-index: 1;
	margin: 145px 0 8px;
    padding: 75px 0;
	background-size: cover;
	background-position: center;
	background-image: url(assets/img/ban2.jpg) !important;
}
	</style>

</head>

<body>
	<!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

	<!--====== Preloader ======-->
	<div id="preloader">
		<div id="loading-center">
			<div id="loading-center-absolute">
				<div class="object" id="object_one"></div>
				<div class="object" id="object_two"></div>
				<div class="object" id="object_three"></div>
				<div class="object" id="object_four"></div>
			</div>
		</div>
	</div>

	<!--====== Header Start ======-->
	<?php   include("header.php")?>
	<!--====== Header End ======-->

	<!--====== Page Title Start ======-->
	<section class="page-title-area">
		<div class="container">
			<div class="row align-items-center justify-content-between">
				<div class="col-lg-8">
					<!-- <h1 class="page-title font-40">Who We Are</h1> -->
				</div>
				<div class="col-auto">
					<ul class="page-breadcrumb">
						<li><a href="index.php">Home</a></li>
						<li>Who We Are</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--====== Page Title End ======-->
	
		<!--====== About Section Start ======-->
		<section class="about-section-three section-gap ">
			<div class="container">
				<div class="row align-items-center justify-content-center mb-150">
					<div class="col-xl-7 col-lg-12 col-md-9 col-sm-10">
						<div class="about-text mb-lg-50 text-justify">
							<div class="common-heading mb-30">
								<span class="tagline">
									<i class="fas fa-plus"></i> About us
									<!-- <span class="heading-shadow-text">About Us</span> -->
								</span>
								<h2 class="title1">Who <span class="highlighter">We</span> Are</h2>
							</div>
							<p><b> Medicinal and Aromatic Plants (MAPs)</b> sector significantly contribute towards the global, national and local economies. MAPs constitute a crucial resource for a wide scale of products, varying from medicines, to food supplements, superfoods, food ingredients, cosmetics and body care products etc. Worldwide recent consumer trends have boosted the demand for natural ingredients as consumers started preferring natural over synthetic.
							</p> 
							<p>India being the key player in the <b>MAPs</b> sector has an important role to play in promotion of sustainability in the sector. Through the <b>Indian Medicinal and Aromatic Plants (I-MAP)</b> - Industry Association, our endeavour is to work with the stakeholders like Government, farmers, collectors, processors as well as businesses and other ecosystem players to promote sustainable MAPs production and consumption. The association facilitates action on value chain capacity building, awareness generation, good practices & knowledge exchange, policy recommendations as well as action towards sustainable transformation of the sector.</p>
							<p> <b> I-MAP </b> act as a key contact point for the medicinal plants industry stakeholders with regard to guidance, interpretation, monitoring and support in implementation of sustainable supply chain initiatives. This would also enable collective efforts towards enhancing the credibility and sustainability performance of Indian medicinal and herbal plant sector. </p>
						</div>
					</div>
					<div class="col-xl-5 col-lg-8 col-md-10">
					<div class="about-gallery wow fadeInRight">
						<div class="img-one">
							<img src="assets/img/about/who-we-are-inner-page.png" alt="Image">
						</div>
					</div>
				</div>
				</div>
			</div>
		</section>
		<!--====== About Section End ======-->



	 <!--====== Footer Start ======-->
	 <?php include("footer.php")?>
    <!--====== Footer End ======-->


	<!--====== jquery js ======-->
</script><script src="assets/js/jquery.min.js"></script>
	<!--====== Bootstrap js ======-->
	<script src="assets/js/bootstrap.min.js"></script>
	<!--====== Inview js ======-->
	<script src="assets/js/jquery.inview.min.js"></script>
	<!--====== Slick js ======-->
	<script src="assets/js/slick.min.js"></script>
	<!--====== Lity js ======-->
	<script src="assets/js/lity.min.js"></script>
	<!--====== Wow js ======-->
	<script src="assets/js/wow.min.js"></script>
	<!--====== Main js ======-->
	<script src="assets/js/main.js"></script>

</body>

</html>