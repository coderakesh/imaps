<!DOCTYPE html>
<html lang="zxx">




<head>
    <!--====== Required meta tags ======-->
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!--====== Title ======-->
    <title> IMAP - Indian Medicinal and Aromatic Plants (I-MAP) - Industry Association || About </title>

    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="img/png" />
    <!--====== Animate Css ======-->
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <!--====== Bootstrap css ======-->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <!--====== Fontawesome css ======-->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
    <!--====== Flaticon css ======-->
    <link rel="stylesheet" href="assets/css/flaticon.css" />
    <!--====== Slick Css ======-->
    <link rel="stylesheet" href="assets/css/slick.min.css" />
    <!--====== Lity Css ======-->
    <link rel="stylesheet" href="assets/css/lity.min.css" />
    <!--====== Main css ======-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <!--====== Responsive css ======-->
    <link rel="stylesheet" href="assets/css/responsive.css" />
</head>

<style>
		.page-title-area {
	position: relative;
	z-index: 1;
	margin: 145px 0 8px;
    padding: 75px 0;
	background-size: cover;
	background-position: center;
	background-image: url(assets/img/ban2.jpg) !important;
}
.project-items .project-item .cats {
    margin-top: 35px;
	margin-bottom: 0px;
}</style>

<body>
    <!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

    <!--====== Preloader ======-->
    <div id="preloader">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object" id="object_one"></div>
                <div class="object" id="object_two"></div>
                <div class="object" id="object_three"></div>
                <div class="object" id="object_four"></div>
            </div>
        </div>
    </div>

    <!--====== Header Start ======-->
    <?php   include("header.php")?>
    <!--====== Header End ======-->

    <!--====== Page Title Start ======-->
    <section class="page-title-area">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-8">
                    <!-- <h1 class="page-title font-40">What We Do</h1> -->
                </div>
                <div class="col-auto">
                    <ul class="page-breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li>Past News</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--====== Page Title End ======-->

    <!--====== About Section Start ======-->
    <section class="project-section section-gap-extra-bottom primary-soft-bg">
		<div class="container">
			<div class="row justify-content-center project-items project-style-two">
				<div class="col-xl-10">
					<div class="project-item mb-30">
						<div class="thumb" style="background-image: url(assets/images/pastnews/MAND/2.jpg);"></div>
						<div class="content">
							
							
							<h5 class="title">
								<a href="pastnews-details.php">Skill Development Training on Medicinal and Aromatic Plants-Mandsaur</a>
							</h5>
							
							<span class="date"><i class="far fa-calendar-alt"></i> 25 February 2021</span>
							<div class="cats">
								<a href="pastnews-details.php">View details</a>
							</div>
						</div>
					</div>
				</div>
                <div class="col-xl-10">
					<div class="project-item mb-30">
						<div class="thumb" style="background-image: url(assets/images/pastnews/SEH/1.jpg);"></div>
						<div class="content">
							
							
							<h5 class="title">
								<a href="pastnews-details1.php">Skill Development Training on Sustainable and Qualitative Production Medicinal Plants-Sehore</a>
							</h5>
							
							<span class="date"><i class="far fa-calendar-alt"></i> 25 February 2021</span>
							<div class="cats">
								<a href="pastnews-details1.php">View details</a>
							</div>
						</div>
					</div>
				</div>

                <div class="col-xl-10">
					<div class="project-item mb-30">
						<div class="thumb" style="background-image: url(assets/images/pastnews/TRAIN/A.jpg);"></div>
						<div class="content">
							
							
							<h5 class="title">
								<a href="pastnews-details2.php">Training on Good Agriculture Practices of voluntary Certification Scheme for Medicinal Plants Produce</a>
							</h5>
							
							<span class="date"><i class="far fa-calendar-alt"></i> 22 Oct 2021</span>
							<div class="cats">
								<a href="pastnews-details2.php">View details</a>
							</div>
						</div>
					</div>
				</div>
			





			</div>
		</div>
	</section>
    <!--====== About Section End ======-->



    <!--====== Footer Start ======-->
    <?php include("footer.php")?>
    <!--====== Footer End ======-->


    <!--====== jquery js ======-->
    <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <!--====== Bootstrap js ======-->
    <script src="assets/js/bootstrap.min.js"></script>
    <!--====== Inview js ======-->
    <script src="assets/js/jquery.inview.min.js"></script>
    <!--====== Slick js ======-->
    <script src="assets/js/slick.min.js"></script>
    <!--====== Lity js ======-->
    <script src="assets/js/lity.min.js"></script>
    <!--====== Wow js ======-->
    <script src="assets/js/wow.min.js"></script>
    <!--====== Main js ======-->
    <script src="assets/js/main.js"></script>

</body>




</html>